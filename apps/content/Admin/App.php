<?php

namespace NewBatu\Admin;

use \Engine5\Core\Engine;
use \Engine5\Core\Engine\Entrypoint;

/**
 * Description of App
 *
 * @author barcis
 */
class App extends \BATUAdmin\Module {

    public function __construct($pathinfo, $method) {
        parent::__construct($pathinfo, $method);
    }

}
