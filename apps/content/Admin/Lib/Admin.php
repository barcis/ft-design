<?php

namespace NewBatu\Admin\Lib;

/**
 * Description of Admin
 *
 * @author barcis
 */
class Admin {

    public function login($username, $password) {
        if (empty($username) || empty($password)) {
            return false;
        }
        $model = \NewBatu\Model\Admin::q()
                ->where('username', $username)
                ->one(['*']);
        
        if (!$model) {
            return false;
        }

        $hash = md5($model->id . $password);
        
        if($model->password !== $hash) {
            return false;
        }
        
        
        return $model;
    }

}
