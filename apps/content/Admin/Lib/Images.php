<?php

namespace NewBatu\Admin\Lib;

/**
 * Description of Images
 *
 * @author barcis
 */
class Images {

    public function transferAritclesImages() {
        $Articles = \NewBatu\Model\Article::q()
                ->whereIsFalse('img_transfer')
                ->limit(1000)
                ->select();

        foreach ($Articles as $Article) {
            echo "Przetwarzam " . $Article->title . "\n";

            $directory = ST_FRONTENDDIR . '/i/a/' . $Article->id . '/';
            if (!file_exists($directory)) {
                echo " * Tworzę katalog " . $directory . "\n";
                mkdir($directory, 0777, true);
                chmod($directory, 0777);
            }

            if (!is_array($Article->images)) {
                echo "Brak zdjęc, pomijam\n";
                $Article->img_transfer = true;
                $Article->save();
                continue;
            }

            try {
                $photos = [];
                foreach ($Article->images as $idx => $photo) {

                    echo " * Zdjęcie " . ($idx + 1) . "\n";
                    $photos[$idx] = $photo;
                    $index = ($idx > 0) ? $idx : '';
                    $append = '';
                    if (!isset($photo->data) && !empty($photo)) {
                        $photo = (object) ['data' => $photo];
                    }

                    if (substr($photo->data, 0, 10) === 'data:image') {
                        //zapisujemy nowe zdjęcie
                        $fn = $Article->rewrite . $index . '.png';

                        while (file_exists($directory . $fn)) {
                            $append .= '_0';
                            $fn = $Article->rewrite . $index . $append . '.png';
                        }

                        $l = strlen(explode(';', $photo->data)[0]) + 8;
                        $code = substr($photo->data, 0, $l);

                        $b64 = substr($photo->data, $l);
                        $blob = base64_decode($b64);

                        $imagick = new \Imagick();
                        $imagick->readImageBlob($blob);


                        $imagick->scaleImage(376, 250, true);

                        file_put_contents($directory . $fn, $imagick->getImageBlob());
                        $photos[$idx] = array('data' => '/i/a/' . $Article->id . '/' . $fn);
                    }
                }
            } catch (\Exception $e) {
                echo " * Nie wyszło :( \n";
            }
            $Article->images = $photos;
            $Article->img_transfer = true;
            $Article->save();
            echo "Zapisuję " . $Article->title . "\n";
        }
    }

    public function transferProductsImages() {
        $Products = \NewBatu\Model\Product::q()
                ->whereIsFalse('img_transfer')
                ->limit(1000)
                ->select();
        foreach ($Products as $Product) {
            echo "Przetwarzam " . $Product->name . "\n";

            $directory = ST_FRONTENDDIR . '/i/p/' . $Product->id . '/';
            if (!file_exists($directory)) {

                echo " * Tworzę katalog " . $directory . "\n";
                mkdir($directory, 0777, true);
                chmod($directory, 0777);
            }

            if (!is_array($Product->photos)) {
                echo "Brak zdjęc, pomijam\n";
                $Product->img_transfer = true;
                $Product->save();
                continue;
            }

            try {
                $photos = [];
                foreach ($Product->photos as $idx => $photo) {

                    echo " * Zdjęcie " . ($idx + 1) . "\n";
                    $photos[$idx] = $photo;
                    $index = ($idx > 0) ? $idx : '';
                    $append = '';
                    if (!isset($photo->data) && !empty($photo)) {
                        $photo = (object) ['data' => $photo];
                    }

                    if (substr($photo->data, 0, 10) === 'data:image') {
                        //zapisujemy nowe zdjęcie
                        $fn = $Product->rewrite . $index . '.png';

                        while (file_exists($directory . $fn)) {
                            $append .= '_0';
                            $fn = $Product->rewrite . $index . $append . '.png';
                        }

                        $l = strlen(explode(';', $photo->data)[0]) + 8;
                        $code = substr($photo->data, 0, $l);

                        $b64 = substr($photo->data, $l);
                        $blob = base64_decode($b64);

                        $imagick = new \Imagick();
                        $imagick->readImageBlob($blob);


                        $imagick->scaleImage(376, 250, true);

                        file_put_contents($directory . $fn, $imagick->getImageBlob());
                        chmod($directory . $fn, 0666);
                        $photos[$idx] = array('data' => '/i/p/' . $Product->id . '/' . $fn);
                    }
                }
            } catch (\Exception $e) {
                echo " * Nie wyszło :( \n";
            }
            $Product->photos = $photos;
            $Product->img_transfer = true;
            $Product->save();
            echo "Zapisuję " . $Product->name . "\n";
        }
    }

}
