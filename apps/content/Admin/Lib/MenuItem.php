<?php

namespace NewBatu\Admin\Lib;

/**
 * Description of MenuItem
 *
 * @author barcis
 */
class MenuItem {

    private $icon = 'fa-dashboard';
    private $text = 'Dashboard';
    private $state = 'home';

    /**
     *
     * @var MenuItemLabel
     */
    private $label = null;

    /**
     *
     * @var MenuItem[]
     */
    private $subitems = [];

    public function __construct($text, $icon = 'fa-dashboard', $state = 'home', $label = null) {
        $this->text = $text;
        $this->icon = $icon;
        $this->label = $label;
        $this->state = $state;
    }

    public function addSubItem(MenuItem $item) {
        return $this->subitems[] = $item;
    }

    public function hasSubItems() {
        return count($this->subitems) > 0;
    }

    public function getSubItems() {
        return $this->subitems;
    }

    public function getText() {
        return $this->text;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function getState() {
        return $this->state;
    }

    public function getLabel() {
        return $this->label;
    }

    public function hasLabel() {
        return !is_null($this->label);
    }

}
