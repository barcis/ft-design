<?php

namespace NewBatu\Admin\Lib;

/**
 * Description of MenuItemLabel
 *
 * @author barcis
 */
class MenuItemLabel {

    private $type = 'label-primary';
    private $text = '4';

    public function __construct($text, $type = 'label-primary') {
        $this->text = $text;
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function getText() {
        return $this->text;
    }

}
