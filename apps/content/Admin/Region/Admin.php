<?php

namespace NewBatu\Admin\Region;

use \Engine5\Core\Form\Field\Text,
    \Engine5\Core\Form\Field\Password,
    \Engine5\Core\Form\Field\Submit,
    \NewBatu\Admin\Lib\MenuItem,
    \NewBatu\Admin\Lib\MenuItemLabel;

/**
 * Description of Admin
 *
 * @author barcis
 */
class Admin extends \Engine5\Core\Region {

    use \Engine5\Helper\Response;

    /**
     * @var \NewBatu\Model\Admin
     */
    public $user;
    public $messages = [];
    public $sidebarMenu = [];

    public function sidebarMenuInit() {
        $this->sidebarMenu[] = new MenuItem('Pulpit', 'fa-dashboard', 'home.dashboard');

        $user = \Engine5\Core\Auth::getLoggedUser();
//
//        if ($user->hasGroup('Admin')) {
//            $admins = new MenuItem('Administratorzy', 'fa-users', 'home.admin.list');
//            $this->sidebarMenu[] = $admins;
//        }
    }

    public function loginInit() {
        if (isset($this->params['logout'])) {
            \Engine5\Core\Auth::logout();
            $this->redirect('NewBatu\Admin\Region\Admin::login', 302);
        }

        $loginForm = $this->form('login-form');

        $loginForm->addFields([
            (new Text('username')),
            (new Password('password')),
                    (new Submit('doLogin', 'Zaloguj'))
                    ->setAction([$this, 'doLoginAction'])
        ]);
    }

    public function sidebarUserInit() {
        $this->user = \Engine5\Core\Auth::getLoggedUser(true);
        $this->auth = \Engine5\Core\Auth::getLoggedUser();
    }

    public function navbarUserInit() {
        $this->user = \Engine5\Core\Auth::getLoggedUser(true);
        $this->auth = \Engine5\Core\Auth::getLoggedUser();
    }

    public function menuInit() {
        $this->user = \Engine5\Core\Auth::getLoggedUser(true);
        $this->auth = \Engine5\Core\Auth::getLoggedUser();
    }

    public function loginDropdownInit() {
        $this->user = \Engine5\Core\Auth::getLoggedUser(true);
    }

    public function doLoginAction() {
        $form = $this->form('login-form');
        if (empty($form->username)) {
            $form->addError('Nie podano nazwy użytkownika');
        }
        if (empty($form->password)) {
            $form->addError('Nie podano hasła');
        }
        if (!$form->hasErrors()) {
            $admin = new \NewBatu\Admin\Lib\Admin();
            if (($mUser = $admin->login($form->username, $form->password)) !== false) {
                $user = new \Engine5\Core\Auth\User($form->username, $mUser->group);
                \Engine5\Core\Auth::Login($user, $mUser);
                $this->redirect('NewBatu\Admin\Region\Admin::dashboard', 302);
            } else {
                $form->addError('Podano nieprawidłowe dane logowania');
            }
        }
    }

    public function passwordInit() {
        $registerForm = $this->form('password-form');
        $registerForm->addElement('password', 'old', 'Aktualne hasło')->required(true);
        $registerForm->addElement('password', 'new', 'Nowe hasło')->required(true);
        $registerForm->addElement('password', 'confirm', 'Potwierdź hasło')->required(true);
        $registerForm->addElement('submit', 'save', '', 'Zapisz', 'passwordAction')->required(true);
    }

    public function passwordAction() {
        $registerForm = $this->form('password-form');

        if ($registerForm->new != $registerForm->confirm) {
            $registerForm->getField('confirm')->addError('Hasła nie są identyczne');
            return;
        }

        $admin = M_admin::findOne('username', \Engine5\Auth::getLoggedUser()->getUsername());

        if ($admin->password != md5($registerForm->old)) {
            $registerForm->getField('old')->addError('Błędne hasło');
            return;
        }

        if ($registerForm->new == $registerForm->old) {
            $registerForm->getField('new')->addError('Hasło jest takie samo jak obecne!');
            return;
        }

        $admin->password = md5($registerForm->new);
        $admin->save();

        $registerForm->addInfo('Hasło zostało zmienione!');
    }

}
