<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb" sng-controller="Controllers.Page.Breadcrumbs">
        <li data-ng-repeat="breadcrumb in breadcrumbs.getAll()" data-ng-class="{'current':$last}">
            <i data-ng-if="$first" class="icon-home"></i>
            <a href="/#{{breadcrumb.url}}" >{{breadcrumb.title}}</a>
        </li>
    </ul>
</div>