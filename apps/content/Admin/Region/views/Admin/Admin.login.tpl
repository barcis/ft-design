<p class="login-box-msg">Zaloguj się aby rozpocząć</p>

[{form name='login-form'}]
<div class="form-group has-feedback">
    [{render name='username' class='form-control' placeholder='Email' autofocus='autofocus'}]
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    [{render name='password' class="form-control" placeholder="Password" }]
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
    <div class="col-xs-8">
        <div class="checkbox icheck">
            <label>
                <input type="checkbox"> Zapamiętaj mnie
            </label>
        </div>
    </div>
    <div class="col-xs-4">
        [{render name='doLogin' class="btn btn-primary btn-block btn-flat"}]
    </div>
</div>
[{/form}]
<a href="#">Zapomniałem hasło</a><br>