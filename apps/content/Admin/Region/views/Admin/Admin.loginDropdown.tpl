<li class="dropdown user">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-male"></i>
        <span class="username">[{$this->user->firstname}] [{$this->user->lastname}]</span>
        <i class="icon-caret-down small"></i>
    </a>
    <ul class="dropdown-menu">
        <li><a href="/admin/[{$this->user->id}].html"><i class="icon-user"></i> Mój profil</a></li>
        <li class="divider"></li>
        <li><a href="/admin/[{$this->user->id}].html"><i class="icon-unlock-alt"></i> Zmiana hasła</a></li>
        <li><a target="_self" href="/logowanie/logout.html"><i class="icon-key"></i> Wyloguj się</a></li>
    </ul>
</li>