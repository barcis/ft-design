<ul id="nav">
    <li data-active-link="current">
        <a href="/">
            <i class="icon-dashboard"></i>
            Dashboard
        </a>
    </li>
    [{if ($this->auth->hasGroups(['Admin', 'HR']))}]
        <li data-active-link="current">
            <a href="/user/">
                <i class="icon-group"></i>
                Użytkownicy
            </a>
            <ul class="sub-menu">
                <li active-link="current">
                    <a href="/user/">
                        <i class="icon-angle-right"></i>
                        Użytkownicy
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/company/">
                        <i class="icon-angle-right"></i>
                        Firmy
                    </a>
                </li>
                [{if ($this->auth->hasGroups(['Admin', 'Payment']))}]
                    <li data-active-link="current">
                        <a href="/paycheck/">
                            <i class="icon-angle-right"></i>
                            Wypłaty
                        </a>
                    </li>
                [{/if}]
            </ul>
        </li>
    [{/if}]
    [{if ($this->auth->hasGroups(['Admin', 'CM']))}]
        <li data-active-link="current">
            <a href="/product/">
                <i class="icon-gift"></i>
                Produkty
            </a>
            <ul class="sub-menu">
                <li active-link="current">
                    <a href="/product/">
                        <i class="icon-angle-right"></i>
                        Produkty
                    </a>
                </li>
                <li active-link="current">
                    <a href="/auction_product/">
                        <i class="icon-angle-right"></i>
                        Produkty aukcji
                    </a>
                </li>
                <li active-link="current">
                    <a href="/param/">
                        <i class="icon-angle-right"></i>
                        Parametry
                    </a>
                </li>
                <li active-link="current">
                    <a href="/tag/">
                        <i class="icon-angle-right"></i>
                        Hashtagi
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/category/">
                        <i class="icon-angle-right"></i>
                        Kategorie
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="product/title/">
                        <i class="icon-angle-right"></i>
                        Tytuły
                    </a>
                </li>
            </ul>
        </li>
    [{/if}]
    [{if ($this->auth->hasGroups(['Admin', 'Redactor']))}]
        <li data-active-link="current">
            <a href="/article/">
                <i class="icon-gift"></i>
                Artykuły
            </a>
            <ul class="sub-menu">
                <li active-link="current">
                    <a href="/article/">
                        <i class="icon-angle-right"></i>
                        Artykuły
                    </a>
                </li>
                <li active-link="current">
                    <a href="/author/">
                        <i class="icon-angle-right"></i>
                        Autorzy
                    </a>
                </li>
                <li active-link="current">
                    <a href="/tag/">
                        <i class="icon-angle-right"></i>
                        Hashtagi
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/article_category/">
                        <i class="icon-angle-right"></i>
                        Kategorie
                    </a>
                </li>
            </ul>
        </li>
    [{/if}]
    [{if ($this->auth->hasGroups(['Admin', 'Import']))}]
        <li active-link="current">
            <a href="/import">
                <i class="icon-money"></i>
                API/XML
            </a>
            <ul class="sub-menu" style="display: none;">
                <li active-link="current">
                    <a href="/import/">
                        <i class="icon-angle-right"></i>
                        API
                    </a>
                </li>
                <li active-link="current">
                    <a href="/import/params.html">
                        <i class="icon-angle-right"></i>
                        Parametry ogólne
                    </a>
                </li>
                <li active-link="current">
                    <a href="/import/shop/">
                        <i class="icon-angle-right"></i>
                        Sklepy
                    </a>
                </li>
            </ul>
        </li>
    [{/if}]
    [{if ($this->auth->hasGroup('Admin'))}]
        <li data-active-link="current">
            <a href="/admin/">
                <i class="icon-group"></i>
                Administratorzy
            </a>
        </li>
    [{/if}]
    [{if ($this->auth->hasGroups(['Admin', 'CMS']))}]
        <li data-active-link="current">
            <a href="/cms/">
                <i class="icon-sitemap"></i>
                Dodatki
            </a>
            <ul class="sub-menu">
                [{if ($this->auth->hasGroup('Admin'))}]
                    <li data-active-link="current">
                        <a href="/mail/">
                            <i class="icon-angle-right"></i>
                            Maile
                        </a>
                    </li>
                [{/if}]
                <li data-active-link="current">
                    <a href="/dvertise/">
                        <i class="icon-angle-right"></i>
                        Reklama
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/slider/">
                        <i class="icon-angle-right"></i>
                        Slider
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/announcements/">
                        <i class="icon-angle-right"></i>
                        Komunikaty
                    </a>
                </li>
                <li data-active-link="current">
                    <a href="/offerpackages/">
                        <i class="icon-angle-right"></i>
                        Pakiety  biznesowe
                    </a>
                </li>
            </ul>
        </li>
    [{/if}]
    [{plugin hook='admin-left-menu'}]
</ul>
