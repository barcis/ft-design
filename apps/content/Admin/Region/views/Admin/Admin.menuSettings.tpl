<!-- Settings -->
<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" title="Ustawienia">
        <i class="fa fa-gears" aria-hidden="true"></i>
    </a>
    <ul class="dropdown-menu">
        [{plugin hook='admin-settings-menu'}]
        <li><a target="_self" href="/logowanie/logout.html"><i class="icon-key"></i> Wyloguj się</a></li>
    </ul>
</li>