<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="/assets/img/user2-160x160.jpg" class="user-image" alt="User Image">
        <span class="hidden-xs">[{$this->user->firstname}] [{$this->user->lastname}]</span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <img src="/assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">

            <p>
                [{$this->user->firstname}] [{$this->user->lastname}]
                <small>Member since Nov. 2012</small>
            </p>
        </li>
        <!-- Menu Body -->
        [{*<li class="user-body">
        <div class="row">
        <div class="col-xs-4 text-center">
        <a href="#">Followers</a>
        </div>
        <div class="col-xs-4 text-center">
        <a href="#">Sales</a>
        </div>
        <div class="col-xs-4 text-center">
        <a href="#">Friends</a>
        </div>
        </div>
        <!-- /.row -->
        </li>*}]
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="/admins/edit-[{$this->user->id}]" class="btn btn-default btn-flat">Profil</a>
            </div>
            <div class="pull-right">
                <a target="_self" href="/logowanie/logout.html" class="btn btn-default btn-flat">Wyloguj</a>
            </div>
        </li>
    </ul>
</li>