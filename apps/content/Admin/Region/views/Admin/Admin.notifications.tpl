<!-- Notifications -->
<li class="dropdown" data-ng-controller="Controllers.Notification.Top">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Masz 5 nowych powiadomień">
        <i data-ng-if="notifications.$resolved" class="icon-warning-sign"></i>
        <i data-ng-if="!notifications.$resolved" class="icon-spin icon-refresh"></i>
        <span data-ng-if="notifications.$resolved" class="badge">{{notifications.length}}</span>
    </a>
    <ul class="dropdown-menu extended notification">
        <li class="title">
            <p>Masz {{notifications.length}} {{notifications.length | variety:'nowe powiadomienie':'nowe powiadomienia':'nowych powiadomień'}}</p>
        </li>
        <li data-ng-repeat="notification in notifications">
            <a href="javascript:void(0);">
                <span class="label label-success"><i class="icon-plus"></i></span>
                <span class="message">New user registration.</span>
                <span class="time">1 mins</span>
            </a>
        </li>

        <li>
            <a href="javascript:void(0);">
                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                <span class="message">High CPU load on cluster #2.</span>
                <span class="time">5 mins</span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="label label-success"><i class="icon-plus"></i></span>
                <span class="message">New user registration.</span>
                <span class="time">10 mins</span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                <span class="message">New items are in queue.</span>
                <span class="time">25 mins</span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="label label-warning"><i class="icon-bolt"></i></span>
                <span class="message">Disk space to 85% full.</span>
                <span class="time">55 mins</span>
            </a>
        </li>
        <li class="footer">
            <a href="javascript:void(0);">Zobacz wszystkie</a>
        </li>
    </ul>
</li>