<div class="page-header" sng-controller="Controllers.Page.Header">
    <div class="page-title">
        <h3>{{breadcrumbs.getLast().title}}</h3>
        <span>Witaj, {{me.firstname}}!</span>
    </div>

    <!-- Page Stats -->
    <!--    <ul class="page-stats">
            <li>
                <div class="summary">
                    <span>New orders</span>
                    <h3>17,561</h3>
                </div>
                <div class="graph circular-chart" data-percent="73">73%</div>
            </li>
            <li>
                <div class="summary">
                    <span>My balance</span>
                    <h3>$21,561.21</h3>
                </div>
                <div class="graph circular-chart" data-percent="73">73%</div>
            </li>
        </ul>-->
    <!-- /Page Stats -->
</div>