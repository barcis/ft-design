<div class="box">
    <h1>Zmiana hasła administratora</h1>
    {form name="password-form" errors=false}
    {foreach $fields as $field}
    <div class="line">
        {if $field->getType() != 'submit'}{label item=$field tags=true}{/if}
        {render item=$field}{errormsg item=$field}
    </div>

    {/foreach}
    {/form}
</div>