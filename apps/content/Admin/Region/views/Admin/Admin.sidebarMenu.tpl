<ul class="sidebar-menu">
    <li class="header">
        Nawigacja
    </li>
    [{foreach $this->sidebarMenu as $item}]
        <li class="treeview" ui-sref-active="active">
            <a  href data-ui-sref="[{$item->getState()}]">
                <i class="fa [{$item->getIcon()}]"></i>
                <span>[{$item->getText()}]</span>
                [{if $item->hasLabel()}]
                    <span class="label pull-right [{$item->getLabel()->getType()}]">[{$item->getLabel()->getText()}]</span>
                [{else}]
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                [{/if}]
            </a>
            [{if $item->hasSubItems()}]
                <ul class="treeview-menu">
                    [{foreach $item->getSubItems() as $subitem}]
                        <li data-ui-sref-active="active">
                            <a href data-ui-sref="[{$subitem->getState()}]">
                                <i class="fa [{$subitem->getIcon()}]"></i>
                                <span>[{$subitem->getText()}]</span>
                                [{if $subitem->hasLabel()}]
                                    <span class="label pull-right [{$subitem->getLabel()->getType()}]">[{$subitem->getLabel()->getText()}]</span>
                                [{/if}]
                            </a>
                        </li>
                    [{/foreach}]
                </ul>
            [{/if}]
        </li>
    [{/foreach}]
    [{plugin hook="admin-sidebar-menu"}]
    [{*<li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>*}]
</ul>