
<div class="sidebar-widget align-center">
    <div class="btn-group" data-toggle="buttons" id="theme-switcher">
        <label class="btn active">
            <input type="radio" name="theme-switcher" data-theme="bright"><i class="icon-sun"></i> Jasne
        </label>
        <label class="btn">
            <input type="radio" name="theme-switcher" data-theme="dark"><i class="icon-moon"></i> Ciemne
        </label>
    </div>
</div>