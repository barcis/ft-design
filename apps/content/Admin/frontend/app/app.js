var app = angular
        .module('NewBatuAdminApp', [
            'ngResource',
            'ngSanitize',
            'chart.js',
            'ui.router',
            'ui.router.util',
            'ct.ui.router.extras',
            'ngContextMenu',
            'notyModule',
            'angularFileUpload',
            'angularMoment',
            'ui.select',
            'ui.select2',
            'ngStorage',
            'ngBootbox',
            'ngBatubox',
            'permission',
            'permission.ui',
            'uiGmapgoogle-maps'
        ]);

app
        .run(function ($rootScope, $filter, $state, amMoment, Engine5) {
            amMoment.changeLocale('pl');
            function makeId()
            {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (var i = 0; i < 5; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                return text;
            }

            $rootScope.Engine5 = Engine5;

            $rootScope.$on('$routeChangeStart', function (event, next, current) {
                $('.tooltip.fade').remove();
                $rootScope.$clearCache();
            });
            var $$loadCacheData = {};
            var $$countCacheData = {};
            $rootScope.$sorting = function (column)
            {
                this.filter.reverse = (this.filter.predicate === column) ? !this.filter.reverse : false;
                this.filter.predicate = column;
            };
            $rootScope.$search = function (input)
            {
                return $filter('filter')(input, this.filter.search);
            };
            $rootScope.$clearCache = function ()
            {
                $$loadCacheData = {};
                $$countCacheData = {};
            };
            $rootScope.$load = function (Model)
            {
                var ctrlId = $state.current.name;
                if (angular.isUndefined(Model))
                    return;
                if (angular.isUndefined(Model.$$uid))
                {
                    Model.$$uid = "aaa" + ctrlId;
                }

                if (angular.isUndefined(Model.$$method))
                {
                    Model.$$method = "query";
                }

                var page = parseInt(this.filter.page || 1);
                var limit = parseInt(this.filter.limit || 10);
                var params = angular.extend({'limit': limit, 'page': page, 'search': this.search, 'sort': this.filter.predicate, 'revers': this.filter.reverse}, Model.$$params);
                var hash = MD5(ctrlId + " " + Model.$$method + " " + Model.$$uid + " " + JSON.stringify(params)).toString();
                if (angular.isUndefined($$loadCacheData[hash]))
                {
                    $$loadCacheData[hash] = Model[Model.$$method](params, function () {
                        if (Model.$$onLoad)
                        {
                            Model.$$onLoad();
                        }
                    });
                }

                return $$loadCacheData[hash];
            };
            $rootScope.$prevPage = function () {
                if (this.filter.page > 1) {
                    this.filter.page--;
                }
            };
            $rootScope.$nextPage = function () {
                var count = this.$count(this.Model).length;
                var maxPage = Math.ceil(count / this.filter.limit);
                var nextPage = (this.filter.page + 1);

                if (nextPage <= maxPage) {
                    this.filter.page = nextPage;
                }


            };
            $rootScope.$count = function (Model)
            {
                if (angular.isUndefined(Model))
                {
                    return;
                }

                if (angular.isUndefined(Model.$$uid))
                {
                    Model.$$uid = makeId();
                }

                if (angular.isUndefined(Model.$$count))
                {
                    Model.$$count = "count";
                }

                var params = angular.extend({'search': this.search}, Model.$$params);
                var hash = MD5(Model.$$count + " " + Model.$$uid + " " + JSON.stringify(params)).toString();
                if (angular.isUndefined($$countCacheData[hash]))
                {
                    $$countCacheData[hash] = Model[Model.$$count](params);
                }

                return $$countCacheData[hash];
            };
        });