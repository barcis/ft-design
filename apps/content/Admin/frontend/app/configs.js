app
        .config(function ($locationProvider, $httpProvider) {
            $locationProvider
                    .html5Mode(true);

            $httpProvider.interceptors.push(function ($q) {
                return {
                    'responseError': function (rejection) {
                        if (rejection.status === 401) {
                            location.replace('/logowanie/');
                            return $q.defer().promise;
                        }
                        return $q.reject(rejection);
                    }
                };
            });
        });

var defaultSuccessMessage = function () {
    noty({
        text: 'Zapisano zmiany',
        type: 'success',
        layout: 'top',
        timeout: 3000
    });
    //App.unblockUI('#content');
};

var defaultFailureMessage = function (data) {
    var message = data.data;

    if ((typeof data.data) === "object" && !angular.isUndefined(data.data.code)) {
        if (data.data.code === '001') {
            message = 'Podana wartość [' + data.data.column + '] istnieje już w bazie. Podaj inną.';
        }
    }

    noty({
        text: message,
        type: 'error',
        layout: 'top',
        timeout: 3000
    });
    //App.unblockUI('#content');
};
