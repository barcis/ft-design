if (!String.prototype.lpad) {
    String.prototype.lpad = function (padString, length) {
        var str = this;
        while (str.length < length)
            str = padString + str;
        return str;
    };
}

if (!String.prototype.strtr) {
    String.prototype.strtr = function (from, to) {
        var fr = '',
                i = 0,
                j = 0,
                lenStr = 0,
                lenFrom = 0,
                tmpStrictForIn = false,
                fromTypeStr = '',
                toTypeStr = '',
                istr = '';
        var tmpFrom = [];
        var tmpTo = [];
        var ret = '';
        var match = false;

        lenStr = this.length;
        lenFrom = from.length;
        fromTypeStr = typeof from === 'string';
        toTypeStr = typeof to === 'string';

        for (i = 0; i < lenStr; i++) {
            match = false;
            if (fromTypeStr) {
                istr = this.charAt(i);
                for (j = 0; j < lenFrom; j++) {
                    if (istr == from.charAt(j)) {
                        match = true;
                        break;
                    }
                }
            } else {
                for (j = 0; j < lenFrom; j++) {
                    if (this.substr(i, from[j].length) == from[j]) {
                        match = true;
                        // Fast forward
                        i = (i + from[j].length) - 1;
                        break;
                    }
                }
            }
            if (match) {
                ret += toTypeStr ? to.charAt(j) : to[j];
            } else {
                ret += this.charAt(i);
            }
        }

        return ret;
    };
}

(function () {

    Date.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    Date.longMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    Date.shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    // defining patterns
    var replaceChars = {
        // Day
        d: function () {
            return (this.getDate() < 10 ? '0' : '') + this.getDate();
        },
        D: function () {
            return Date.shortDays[this.getDay()];
        },
        j: function () {
            return this.getDate();
        },
        l: function () {
            return Date.longDays[this.getDay()];
        },
        N: function () {
            return this.getDay() + 1;
        },
        S: function () {
            return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th')));
        },
        w: function () {
            return this.getDay();
        },
        z: function () {
            var d = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((this - d) / 86400000);
        }, // Fixed now
        // Week
        W: function () {
            var d = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7);
        }, // Fixed now
        // Month
        F: function () {
            return Date.longMonths[this.getMonth()];
        },
        m: function () {
            return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1);
        },
        M: function () {
            return Date.shortMonths[this.getMonth()];
        },
        n: function () {
            return this.getMonth() + 1;
        },
        t: function () {
            var d = new Date();
            return new Date(d.getFullYear(), d.getMonth(), 0).getDate()
        }, // Fixed now, gets #days of date
        // Year
        L: function () {
            var year = this.getFullYear();
            return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0));
        }, // Fixed now
        o: function () {
            var d = new Date(this.valueOf());
            d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3);
            return d.getFullYear();
        }, //Fixed now
        Y: function () {
            return this.getFullYear();
        },
        y: function () {
            return ('' + this.getFullYear()).substr(2);
        },
        // Time
        a: function () {
            return this.getHours() < 12 ? 'am' : 'pm';
        },
        A: function () {
            return this.getHours() < 12 ? 'AM' : 'PM';
        },
        B: function () {
            return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24);
        }, // Fixed now
        g: function () {
            return this.getHours() % 12 || 12;
        },
        G: function () {
            return this.getHours();
        },
        h: function () {
            return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12);
        },
        H: function () {
            return (this.getHours() < 10 ? '0' : '') + this.getHours();
        },
        i: function () {
            return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
        },
        s: function () {
            return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds();
        },
        u: function () {
            var m = this.getMilliseconds();
            return (m < 10 ? '00' : (m < 100 ?
                    '0' : '')) + m;
        },
        // Timezone
        e: function () {
            return "Not Yet Supported";
        },
        I: function () {
            var DST = null;
            for (var i = 0; i < 12; ++i) {
                var d = new Date(this.getFullYear(), i, 1);
                var offset = d.getTimezoneOffset();

                if (DST === null)
                    DST = offset;
                else if (offset < DST) {
                    DST = offset;
                    break;
                } else if (offset > DST)
                    break;
            }
            return (this.getTimezoneOffset() == DST) | 0;
        },
        O: function () {
            return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00';
        },
        P: function () {
            return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00';
        }, // Fixed now
        T: function () {
            return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1');
        },
        Z: function () {
            return -this.getTimezoneOffset() * 60;
        },
        // Full Date/Time
        c: function () {
            return this.format("Y-m-d\\TH:i:sP");
        }, // Fixed now
        r: function () {
            return this.toString();
        },
        U: function () {
            return this.getTime() / 1000;
        }
    };

    // Simulates PHP's date function
    Date.prototype.format = function (format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function (_, esc, chr) {
            return (esc === '' && replaceChars[chr]) ? replaceChars[chr].call(date) : chr;
        });
    };

}).call(this);

var MEMBER_NAME_REGEX = /^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;

function isValidDottedPath(path) {
    return (path != null && path !== '' && path !== 'hasOwnProperty' &&
            MEMBER_NAME_REGEX.test('.' + path));
}

function lookupDottedPath(obj, path) {
    if (!isValidDottedPath(path)) {
        throw $resourceMinErr('badmember', 'Dotted member path "@{0}" is invalid.', path);
    }
    var keys = path.split('.');
    for (var i = 0, ii = keys.length; i < ii && obj !== undefined; i++) {
        var key = keys[i];
        obj = (obj !== null) ? obj[key] : undefined;
    }
    return obj;
}

Array.prototype.findIndex = function (property, value) {
    var len = this.length;
    for (var n = 0; n < len; n++) {
        var i = this[n];
        if (lookupDottedPath(i, property) === value) {
            return n;
        }
    }
    return -1;
};

var pd = function () {
    console.log('PD:', arguments);
};