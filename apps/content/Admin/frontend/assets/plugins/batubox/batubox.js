(function($) {

    var defaults = {
        templates: {
            loader: '<div class="batubox-loader"></div>',
            overlay: '<div class="batubox-overlay"></div>',
            container: '<div class="batubox-containter"></div>',
            wrap: ' <div class="batubox-wrap"></div>',
            windows: '<div class="batubox-windows"></div>',
            window: '<div class="batubox-window"></div>',
            windowContent: '<div class="batubox-window-content"></div>',
            windowFooter: '<div class="batubox-window-footer"> © Copyrights BATUSystems 2014. Wszystkie prawa zastrzeżone. </div>'
        },
        width: 'auto',
        height: 'auto'
    }, $wrap, $overlay, $container, $windowContent, $windowFooter, windows = {}, batubox = function() {
        this.version = '0.0.1';
    }, onkeyClose = function(e) {
        if (e.which === 27) {
            functions.close('$$last');
        }
    }, functions = {
        closeOverlay: function() {
            if (windows.length === 0) {
                $("body").css({'overflow-y': 'visible'});
                $container.remove();
                $wrap = null;
                $overlay = null;
                $container = null;
                $windowContent = null;
                $windowFooter = null;
            }
            else {
                $wrap.append(windows.last);
            }

            if ((typeof afterClose) === 'function') {
                afterClose();
            }
        },
        close: function(ID, afterClose, beforeClose) {
            if ((typeof ID) === 'function') {
                beforeClose = afterClose;
                afterClose = ID;
                delete ID;
            }

            if ($container) {
                if ((typeof beforeClose) === 'function') {
                    beforeClose();
                }

                if ((typeof ID) !== 'undefined') {
                    var id = (ID === '$$last') ? (((typeof windows.last) !== 'undefined') ? windows.last.ID : null) : ID;
                    var $window = ((typeof windows[id]) !== 'undefined') ? windows[id] : null;

                    if ($window) {
                        var options = $window.options;

                        if ((typeof options.beforeClose) === 'function') {
                            options.beforeClose(options);
                        }

                        $window.fadeOut(250, function() {
                            $window.remove();
                            delete windows[id];

                            if ((typeof options.afterClose) === 'function') {
                                options.afterClose(options);
                            }

                            functions.closeOverlay();
                        });
                    }
                }
                else if ((typeof ID) === 'undefined') {
                    for (id in windows) {
                        if (windows.hasOwnProperty(id)) {

                            var $window = windows[id];
                            var options = $window.options;

                            if ((typeof options.beforeClose) === 'function') {
                                options.beforeClose(options);
                            }

                            $window.remove();
                            delete windows[id];

                            if ((typeof options.afterClose) === 'function') {
                                options.afterClose(options);
                            }
                        }
                    }
                }
            }
        }
    };
    Object.defineProperty(windows, "length", {
        get: function() {
            var size = 0, key;
            for (key in this) {
                if (this.hasOwnProperty(key))
                    size++;
            }
            return size;
        }
    });
    Object.defineProperty(windows, "last", {
        get: function() {
            var last, key;
            for (key in this) {
                if (this.hasOwnProperty(key))
                    last = this[key];
            }
            return last;
        }
    });
    $.batubox = new batubox();

    $.extend($.batubox, {
        getWindow: function(ID) {
            if ((typeof ID) !== 'undefined' && ID === '$$last') {
                return windows.last;
            }
            else if ((typeof ID) !== 'undefined' && (typeof windows[ID]) !== 'undefined') {
                return windows[ID];
            }
            return null;
        },
        open: function(_options, afterOpen, beforeOpen) {
            $.batubox.showLoading();
            if ((typeof _options) === 'function') {
                beforeOpen = afterOpen;
                afterOpen = _options;
                _options = {};
            }

            if ((typeof beforeOpen) === 'function') {
                beforeOpen();
            }

            var options = this.options = $.extend({}, defaults, _options);
            $container = $container || $(options.templates.container);
            $overlay = $overlay || $(options.templates.overlay);
            if (!$wrap) {


                $wrap = $(options.templates.wrap);

                $('body').on('keyup', onkeyClose);

                $wrap.on('click', function(e) {
                    if (e.target !== this) {
                        return;
                    }
                    functions.close('$$last');
                    return false;
                });
            }
            $container.append($overlay);
            $container.append($wrap);
            if ((typeof options.id) === 'undefined') {
                options.id = Math.random().toString(36).substring(7);
            }

            options.content = options.content || $('<div>BATUBox no content!</div>');
            $overlay.append(windows.last);
            var $window = windows[options.id] = $(options.templates.window);
            $window.ID = options.id;
            $windowContent = $(options.templates.windowContent);
            $windowFooter = $(options.templates.windowFooter);
            $window
                    .append($windowContent)
                    .append($windowFooter);
            $wrap.append($window);
            $window.css({'visibility': 'hidden', 'position': 'static'});
            $("body").css({'overflow-y': 'hidden'}).append($container);
            if ((typeof options.beforeOpen) === 'function') {
                options.beforeOpen(options);
            }

            $windowContent.html(options.content);
            if ((typeof options.afterOpen) === 'function') {
                options.afterOpen(options);
            }
            $window.options = options;
            var width = (options.width === 'auto') ? $window.width() : options.width;
            var height = (options.height === 'auto') ? $window.height() : options.height;
            options.width = width;
            options.height = height;
            $.batubox.hideLoading();
            $window.css({'visibility': 'visible', 'display': 'none', 'position': 'absolute', 'width': width, 'height': height}).fadeIn(250, function() {
                if ((typeof afterOpen) === 'function') {
                    afterOpen();
                }
            });


            return {ID: $window.ID, content: options.content};
        },
        close: function(ID, afterClose, beforeclose) {
            functions.close(ID, afterClose, beforeclose);
        },
        showLoading: function(css) {
            var $indicator = $('#batubox-indicator');
            if ($indicator.length === 0) {
                $indicator = $('<div id="batubox-indicator"></div>');
                $indicator.hide();
                if (css) {
                    $indicator.css(css);
                }
                $('body').append($indicator);
                $indicator.fadeIn(250);
            }
            return $indicator;
        },
        hideLoading: function() {
            var $indicator = $('#batubox-indicator');
            if ($indicator.length === 0)
            {
                return false;
            }

            $indicator.fadeOut(250, function() {
                $indicator.remove();
            });
            return true;
        }
    });
    $.fn.batubox = function(options) {
        if ((typeof this.batubox.ID) !== 'undefined') {
            return;
        }

        options = options || {};
        options.content = this;
        var w = $.batubox.open(options);
        $.extend(this.batubox, {
            ID: w.ID,
            close: function() {
                $.batubox.close(w.ID);
            }
        });
        return this;
    };
}(jQuery));
