$(function() {
    $('.wysiwyg').each(function() {
        var h = $(this).height() + 'px', w = $(this).width() + 'px';
        $(this).ckeditor({
            height: h,
            width: w,
            entities: false,
            filebrowserBrowseUrl: '/js/kcfinder/browse.php?type=files',
            filebrowserImageBrowseUrl: '/js/kcfinder/browse.php?type=images',
            filebrowserFlashBrowseUrl: '/js/kcfinder/browse.php?type=flash',
            filebrowserUploadUrl: '/js/kcfinder/upload.php?type=files',
            filebrowserImageUploadUrl: '/js/kcfinder/upload.php?type=images',
            filebrowserFlashUploadUrl: '/js/kcfinder/upload.php?type=flash',
            toolbar: 'Admin',
            toolbar_Admin: [
                {name: 'document', items: ['Source', '-', 'Save', 'DocProps', 'Preview', 'Print']},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote',
                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar']},
                '/',
                {name: 'styles', items: ['Format', 'Font', 'FontSize']},
                {name: 'tools', items: ['About']}
            ]
        });
    });
});