<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <base href="/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>AdminLTE 2 | Log in</title>
        <link href="/assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/jvectormap/jquery-jvectormap.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/iCheck/skins/square/blue.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="/">
                    <img src="http://squad.com.pl/assets/img/logo.png" alt="squad"/>
                </a>
            </div>
            <div class="login-box-body">
                [{region name='\NewBatu\Admin\Region\Admin' view='login'}]
            </div>
        </div>

        <script src="/assets/plugins/jquery/dist/jquery.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap/dist/js/bootstrap.js" type="text/javascript"></script>
        <script src="/assets/plugins/iCheck/icheck.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });
            });
        </script>
    </body>
</html>
