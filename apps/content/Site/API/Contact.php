<?php

namespace NewBatu\Site\API;

use Engine5\Core\Rest\Api;

class Contact extends Api {

    use \Engine5\Helper\Request;

    public function createOne() {
        if (isset($this->data->message)) {


            $sender = $this->data->sender;
            $senderEmail = $this->data->senderEmail;
            $message = $this->data->message;
            $company = $this->data->company;
            $phone = $this->data->phone;
            $topic = $this->data->topic;
            $errors = [];
//         Title
            if (!isset($this->data->topic) || strlen($this->data->topic) === 0) {
                $errors[] = "TITLE_REQUIRED";
            } elseif (strlen($this->data->topic) < 3) {
                $errors[] = "TITLE_TOO_SHORT";
            }
//        Text
            if (!isset($this->data->message) || strlen($this->data->message) === 0) {
                $errors[] = "CONTENT_REQUIRED";
            } elseif (strlen($this->data->message) < 3) {
                $errors[] = "CONTENT_TOO_SHORT";
            }
//        Name
            if (!isset($this->data->sender) || strlen($this->data->sender) === 0) {
                $errors[] = "NAME_REQUIRED";
            } elseif (strlen($this->data->sender) < 3) {
                $errors[] = "NAME_TOO_SHORT";
            }
//        Email
            if (!isset($this->data->senderEmail) || strlen($this->data->senderEmail) === 0) {
                $errors[] = "EMAIL_REQUIRED";
            } elseif (!preg_match('/[0-9a-z\_\.\-]+\@[0-9a-z\.\-]+/', $this->data->senderEmail)) {
                $errors[] = "EMAIL_BAD_FORMAT";
            }
//        Phoone
            if (!isset($this->data->phone) || strlen($this->data->phone) === 0) {
                $errors[] = "PHONE_REQUIRED";
            } elseif (!preg_match('/^[0-9\-]|[\+0-9]|[0-9\s]|[0-9()]*$/', $this->data->phone)) {
                $errors[] = "PHONE_BAD_FORMAT";
            }


            if (count($errors) > 0) {
                return \Engine5\Core\Rest\Results::instance()->DataValidationFaild($errors);
            } else {
                $mail = new \Engine5\Tools\Mail();


                $mail->setFromName("Wiadomość ze stony internetowej");
                $mail->addTo('yuliia.oliinyk@batusystems.pl', 'FTDesign');
                $mail->setSubject = $topic;
                $content = "<b>Imię: </b> $sender<br/>"
                        . "<br/><b>Email: </b> $senderEmail<br/>"
                        . "<br/><b>Firma: </b> $company<br/>"
                        . "<br/><b>Telefon: </b> $phone<br/>"
                        . "<br/><b>Temat: </b> $topic<br/>"
                        . "<br/><b>Widomość: </b> $message<br/>";
                $mail->setContent($content);
//            pd($content);
                $mail->send();
                return \Engine5\Core\Rest\Results::instance()->NoContent(null);
            }
        }
    }

}
