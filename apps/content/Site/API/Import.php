<?php

namespace NewBatu\Site\API;

use Facebook;
use Engine5\Core\Rest\Api;

class Import extends Api {

    use \Engine5\Helper\Request;

    public function importFacebook() {

        $social_type_id = 2;

        $app_id = '967241603374497';
        $app_secret = '08354468005c717a266c0b3dd4c2998e';
        $client_token = 'EAANvs3wuXaEBAGFeJGAnZBrg8rrXgvvL84tpwsLPNuLkMP5hCZCfemNonZAGsWVKW1lV0jsmeu64HyvMYyOXqqlrzPnKDNf8QXERIaU47Gvr8Bplcl7BP4uaCZAkG43ZCORNBzkOg8XjPZBPUxid4u5XIUY6vkHZCJtTvVrSdfC9QZDZD';

        $fb = new Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.6'
        ]);

        $fb->setDefaultAccessToken($client_token);

        try {
            $response = $fb->get('/106509056367054/posts?fields=id,created_time,description,from,full_picture,name,message,type,source,link,permalink_url', $client_token);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returSned an error: ' . $e->getMessage();
            exit;
        }
        $pagesEdge = $response->getGraphEdge();

        $posts = [];
        foreach ($pagesEdge as $page) {
            $posts[] = $page->asArray();
            if (count($posts) == 5) {
                break;
            }
        }

        return \Engine5\Core\Rest\Results::instance()->Ok($posts);
    }

}
