<?php

namespace NewBatu\Site\API;

use \Engine5\Core\Rest\Result;

class Urls extends \Engine5\Core\Rest\Api {

    public function translate() {
        $this->data = (object) $_GET;
        if (!isset($this->data->app)) {
            return new Result('Nie podana aplikacji!', 404);
        }

        $router = \Engine5\Factory\Router::newInstance([], 'Standard');


        $bases = $router->getBaseForAplication($this->data->app);

        return \Engine5\Core\Rest\Results::instance()->Ok($bases);
    }

}
