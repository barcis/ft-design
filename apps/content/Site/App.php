<?php

namespace NewBatu\Site;

/**
 * Description of App
 *
 * @author barcis
 */
class App extends \Engine5\Core\App {

    use \Engine5\Helper\Angular;

    public function __construct($pathinfo, $method) {
        parent::__construct($pathinfo, $method);

        \Engine5\Core\Auth::setProfix('public');

        $this->loadAngularApp();


        if ($method === 'GET') {
            $params = \Engine5\Core\Request::getParams();
            if (
                    !isset($_COOKIE['partner_id']) &&
                    isset($params['get']) &&
                    isset($params['get']['referer']) &&
                    preg_match('/partner-(?<partner_id>[0-9]+)/', $params['get']['referer'], $matches)) {

                $partner_id = (int) $matches['partner_id'];
                $count = \NewBatu\Model\User::q()->where('id', $partner_id)->count();

                if ($count === 1) {
                    setcookie('partner_id', $partner_id, time() + (86400 * 30), '/');
                }
            }
        }
    }

}
