<?php

namespace NewBatu\Site\Region;

class Errors extends \Engine5\Core\Region {

    public function e404Init() {
        http_response_code(404);
    }

}
