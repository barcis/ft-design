<?php

namespace NewBatu\Site\Region;

/**
 * Description of Metatags
 *
 * @author barcis
 */
class Metatags extends \CMS\Region\Metatags {

    public $title = 'BATU';
    public $description = 'BATU';
    public $keywords = 'BATU';

    public function defaultInit() {
        parent::defaultInit();
    }

}
