<div class="container">
    <section class="text">
        <article>
            <h1>
                [{content name="title"}]
            </h1>
            <div>
                [{content name="content"}]
            </div>
        </article>
    </section>
</div>