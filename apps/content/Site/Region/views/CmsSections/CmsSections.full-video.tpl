<div class="container-fluid">
    <section>
        <div class="videoWrapper">
            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/[{content name="video_id"}]?&autoplay=1&loop=1&rel=0&showinfo=0&color=white&iv_load_policy=3&playlist=[{content name="video_id"}]" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </section>
</div>