<section class="container-fluid">
    <div class="row">
        <div class="gallery">
            [{foreach $this->sections as $cmsSection}]
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
                    <div class="item" data-name="photo">
                        [{region name='NewBatu\Site\Region\CmsSections' view=$cmsSection->type->name section=$cmsSection}]
                    </div>
                </div>
            [{/foreach}]

        </div>
    </div>
</section>