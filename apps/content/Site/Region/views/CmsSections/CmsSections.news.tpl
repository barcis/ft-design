
<div class="parallax photo-news"></div>
<div class="container" data-ng-controller="Controllers.News.List">
    <div class=" microblog">
        <div ng-repeat="item in Model" class="microblog-item" ng-switch="item.type" >
                <a href="{{item.permalink_url}}" target="_blank" class="fb-link">
            <div ng-switch-when="photo">
                <div class="picture">
                    <img class="fb-img" alt="{{item.name}}" src="{{item.full_picture}}">
                </div>
                <div class="item-title">
                    {{item.name}}
                </div>
                <div class="desc">
                     {{item.message}}
                </div>
            </div>
            <div ng-switch-when="video">
                <div class="picture">
                    <iframe width="286" height="160" frameborder="0" allowfullscreen autoplay="0" data-ng-src="{{item.trustedSource}}"></iframe>
                </div>
                <div class="item-title">
                 {{item.name}}
                </div>
                <div class="desc">
                   
                     {{item.message}} 
                </div>
            </div>    
            <div ng-switch-when="status">
                <div class="item-title">
                  {{item.name}}
                </div>
                <div class="desc">
                     {{item.message}}
                </div>
            </div>
            <div ng-switch-when="event">
                <div class="picture">
                    <img class="fb-img" alt="{{item.name}}" src="{{item.full_picture}}">
                </div>
                <div class="item-title">
                    {{item.name}}
                </div>
                <div class="desc">
                     {{item.message}} 
                </div>
            </div>
            <div ng-switch-when="link">
                <div class="picture">
                    <img class="fb-img" alt="{{item.name}}" src="{{item.full_picture}}">
                </div>
                <div class="item-title">
                    {{item.name}}
                </div>
                <div class="desc">
                     {{item.message}} 
                </div>
            </div>
            <div ng-switch-when="offer">
                <div class="picture">
                    <img class="fb-img" alt="{{item.name}}" src="{{item.full_picture}}">
                </div>
                <div class="item-title">
                    {{item.name}}
                </div>
                <div class="desc">
                     {{item.message}} 
                </div>
            </div>
                </a>
[{*       <pre>{{item|json}}</pre>*}]
            
            <div class="social">
                [{*<span>Udostępnij</span>*}]
                <a class="shareLink google" target="_blank" href="https://plus.google.com/share?url={{item.permalink_url}};mode=page"><i class="fa fa-google-plus fa-lg" style="color:#CB2028"></i></a>
                <a class="shareLink twitter" target="_blank" href="http://twitter.com/share?text=&amp;url={{item.permalink_url}};mode=page"><i class="fa fa-twitter fa-lg" style="color:#00aced"></i></a>
                <a class="shareLink fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{item.permalink_url}};mode=page"><i class="fa fa-facebook fa-lg" style="color:#3b5998"></i></a>
            </div>
                <div class="author">
              <span >{{item.from.name}}</span>
                <span class="dateYmd">{{item.dateYmd |date:'y-MM-dd'}}</span>
                </div>
        </div>
    </div>
</div>
