<div class="container">
    [{foreach $this->sections as $cmsSection}]
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            [{region name='NewBatu\Site\Region\CmsSections' view=$cmsSection->type->name section=$cmsSection}]
        </div>
    [{/foreach}]
</div>
