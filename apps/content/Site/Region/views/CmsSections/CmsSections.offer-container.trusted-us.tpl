<div class="container">
    [{ifcontent name="logo"}]
    <div title="[{content name="name"}]">
        <img src="[{content name="logo"}]" alt="[{content name="name"}]">
    </div>
    [{/ifcontent}]
</div>