<section class="batu-acordeon hidden-md hidden-sm hidden-xs">
    <div class="mySlider">
        <ul>
            [{foreach $this->sections as $cmsSection}]
                [{region name='NewBatu\Site\Region\CmsSections' view=$cmsSection->type->name section=$cmsSection}]
            [{/foreach}]
        </ul>
    </div>
</section>
