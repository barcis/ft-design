<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed  page-scroll" data-toggle="collapse" data-target="#hamburger" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="logo" href="/">
                <img src="/assets/img/Logo-main.png" alt="Logo BATUSystems"/>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="hamburger">
            <div class=" nav navbar-nav navbar-right">
                <ul>
                    <li><a class="page-scroll" href="#home">Główna</a></li>
                    <li><a class="page-scroll" href="#about-us">O firmie</a></li>
                    <li><a class="page-scroll" href="#offer">Oferta</a></li>
                    <li><a class="page-scroll" href="#portfolio">Realizacje</a></li>
                    <li><a class="page-scroll" href="#technologies">Technologie</a></li>
                    <li><a class="page-scroll" href="#contact">Kontakt</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>