var app = angular
        .module('FTDesignApp', [
            'ngResource'
        ]);
app
        .run(function ($location, $anchorScroll) {
            if ($location.hash()) {
                $anchorScroll();
            }
        });