Controllers = Controllers || {};
Controllers.Contact = function () {
};
angular.extend(Controllers.Contact, {
    SendContact: function ($scope, NotyService, Contact) {

        $scope.ask = new Contact();
        $scope.phonemask = {
            "mask": "999-999-999"
        };

        $scope.submit = function () {
            if (!angular.isString($scope.ask.sender) || $scope.ask.sender.length < 3) {
                NotyService.error('Podaj Imię i Nazwisko.');
                return;
            }
            if (!angular.isString($scope.ask.senderEmail) || $scope.ask.senderEmail.length < 3) {
                NotyService.error('Uzupełnij email.');
                return;
            } else if (!$scope.ask.senderEmail.match(/[0-9a-z\_\.\-]+\@[0-9a-z\.\-]+/)) {
                NotyService.error('Email nie jest porawny.');
                return;
            }
            if ($scope.ask.phone) {
                var phone = $scope.ask.phone.replace(/[^0-9]/gi, "");
                if (phone.length < 9) {
                    NotyService.error('Uzupełnij numer telefonu.');
                    return;
                }
            } else {
                NotyService.error('Podaj numer telefonu.');
                return;
            }
            if (!angular.isString($scope.ask.topic) || $scope.ask.topic.length < 3) {
                NotyService.error('Uzupełnij tytuł zapytania.');
                return;
            }
            if (!angular.isString($scope.ask.message) || $scope.ask.message.length < 3) {
                NotyService.error('Uzupelnij brakującą treść.');
                return;
            }

            $scope.ask.$create(function () {
                NotyService.success('Twoja wiadomość została pomyślnie przesłana!');
            });

        };
    }
});

app.controller('Controllers.Contact', Controllers.Contact);
app.controller('Controllers.Contact.SendContact', Controllers.Contact.SendContact);
