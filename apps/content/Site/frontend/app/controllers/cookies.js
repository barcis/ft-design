Controllers = Controllers || {};

Controllers.Cookies = function ($scope, $cookieStore, $cookies) {
    $scope.show = angular.isUndefined($cookieStore.get('ACCEPTED'));
    $scope.click = function () {
        $scope.show = false;
        $cookieStore.put('ACCEPTED', 'yes');
    };
};

app.controller('Controllers.Cookies', Controllers.Cookies);