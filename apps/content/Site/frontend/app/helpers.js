if (!String.prototype.strtr) {
    String.prototype.strtr = function (from, to) {
        var fr = '',
                i = 0,
                j = 0,
                lenStr = 0,
                lenFrom = 0,
                tmpStrictForIn = false,
                fromTypeStr = '',
                toTypeStr = '',
                istr = '';
        var tmpFrom = [];
        var tmpTo = [];
        var ret = '';
        var match = false;

        lenStr = this.length;
        lenFrom = from.length;
        fromTypeStr = typeof from === 'string';
        toTypeStr = typeof to === 'string';

        for (i = 0; i < lenStr; i++) {
            match = false;
            if (fromTypeStr) {
                istr = this.charAt(i);
                for (j = 0; j < lenFrom; j++) {
                    if (istr == from.charAt(j)) {
                        match = true;
                        break;
                    }
                }
            } else {
                for (j = 0; j < lenFrom; j++) {
                    if (this.substr(i, from[j].length) == from[j]) {
                        match = true;
                        // Fast forward
                        i = (i + from[j].length) - 1;
                        break;
                    }
                }
            }
            if (match) {
                ret += toTypeStr ? to.charAt(j) : to[j];
            } else {
                ret += this.charAt(i);
            }
        }

        return ret;
    };
}


var MEMBER_NAME_REGEX = /^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;

function isValidDottedPath(path) {
    return (path != null && path !== '' && path !== 'hasOwnProperty' &&
            MEMBER_NAME_REGEX.test('.' + path));
}

function lookupDottedPath(obj, path) {
    if (!isValidDottedPath(path)) {
        throw $resourceMinErr('badmember', 'Dotted member path "@{0}" is invalid.', path);
    }
    var keys = path.split('.');
    for (var i = 0, ii = keys.length; i < ii && obj !== undefined; i++) {
        var key = keys[i];
        obj = (obj !== null) ? obj[key] : undefined;
    }
    return obj;
}

Array.prototype.findIndex = function (property, value) {
    var len = this.length;
    for (var n = 0; n < len; n++) {
        var i = this[n];
        if (lookupDottedPath(i, property) === value) {
            return n;
        }
    }
    return -1;
};

function getScrollOffsets(w) {

    // Use the specified window or the current window if no argument
    w = w || window;

    // This works for all browsers except IE versions 8 and before
    if (w.pageXOffset !== null)
        return {
            x: w.pageXOffset,
            y: w.pageYOffset
        };

    // For IE (or any browser) in Standards mode
    var d = w.document;
    if (document.compatMode === "CSS1Compat") {
        return {
            x: d.documentElement.scrollLeft,
            y: d.documentElement.scrollTop
        };
    }

    // For browsers in Quirks mode
    return {
        x: d.body.scrollLeft,
        y: d.body.scrollTop
    };
}

function strip_tags(input, allowed) {
    allowed = (((allowed || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
            .replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
}

var batujs = function () {
    this.version = '0.0.1';

    this.checkdate = function (m, d, y) {
        return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0))
                .getDate();
    };
};

var _$ = batujs = new batujs();