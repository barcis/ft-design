app
        .value('Messages', {
            Account: {
                Login: {
                    Success: '',
                    Failure: {
                        'NoEmailAndPassword': 'Proszę podać email i hasło',
                        'NoEmail': 'Proszę podać email',
                        'InvalidEmail': 'Nieprawidłowy adres email',
                        'NoPassword': 'Proszę podać hasło',
                        'PasswordIncorrect': 'Stare hasło niepoprawne',
                        'LoginOrPasswordIncorrect': 'Login lub hasło niepoprawne',
                        'NoActive': 'Twoje konto jest nieaktywne'
                    }
                },
                Register: {
                    Success: '',
                    Failure: {
                        'NoEmail': 'Proszę podać email',
                        'InvalidEmail': 'Nieprawidłowy adres email',
                        'NoPassword': 'Proszę podać hasło',
                        'NoRePassword': 'Proszę powtórzyć hasło',
                        'OtherRePassword': 'Podane hasła się różnią',
                        'Terms': 'Wymagana akceptacja warunków',
                        'MailExists': 'Podany email jest już zarejestrowany',
                        'WrongPassword': 'Hasło powinno mieć długość min. 8 znaków, zawierać co najmniej po jednej cyfrze, znaku specjalnym (np. %, $, #, @, !) i wielkiej literze',
                    }
                },
                Recovery: {
                    Success: '',
                    Failure: {
                        'NoEmail': 'Proszę podać email',
                        'InvalidEmail': 'Nieprawidłowy adres email',
                        'NoAccount': 'Nie znaleziono konta dla podanego adresu',
                    }
                },
                ResetPassword: {
                    Success: '',
                    Failure: {
                        'NoPassword': 'Proszę podać hasło',
                        'NoRePassword': 'Proszę powtórzyć hasło',
                        'OtherRePassword': 'Podane hasła się różnią',
                        'WrongPassword': 'Hasło powinno mieć długość min. 8 znaków, zawierać co najmniej po jednej cyfrze, znaku specjalnym (np. %, $, #, @, !) i wielkiej literze',
                    }
                }
            }
        });