app
        .factory('Contact', function ($resource) {
            var Contact = $resource('/rest/contact.json', {}, {
                create: {method: 'POST', isArray: false}
            });

            return Contact;
        });
