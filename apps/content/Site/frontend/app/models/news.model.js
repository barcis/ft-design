app
        .factory('News', function ($resource, $sce) {
            var News = $resource('/rest/news.json', {}, {
                query: {method: 'GET', isArray: true}
            });

            Object.defineProperty(News.prototype, "trustedSource", {
                enumerable: true,
                get: function () {
                    var yt = this.source;
                    yt = yt.replace('autoplay=1', 'autoplay=0');
                    return $sce.trustAsResourceUrl(yt);
                }
            });
            
            
            
             Object.defineProperty(News.prototype, 'dateYmd', { 
                get: function () { 
                    if (angular.isUndefined(this._dateYmd)) { 
                        this._dateYmd = new Date(this.created_time.date); 
                    } 
 
                    return this._dateYmd; 
                } 
            }) ;
            
            
            
            
            

            return News;
        });
