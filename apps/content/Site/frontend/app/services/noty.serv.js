app.service('NotyService', function () {

    this.alert = function (text) {
        new Noty({text: text, type: 'success'}).show();
    };
    this.success = function (text) {
        new Noty({text: text, type: 'success'}).show();
    };
    this.warning = function (text) {
        new Noty({text: text, type: 'warning'}).show();
    };
    this.error = function (text) {
        new Noty({text: text, type: 'error'}).show();
    };
    this.info = function (text) {
        new Noty({text: text, type: 'info'}).show();
    };
});