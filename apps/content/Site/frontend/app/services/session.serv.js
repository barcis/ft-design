app.service('SessionService', function ($rootScope, Account) {
    var sessionObject = {
        'currentUser': Account.get(),
        'isLogged': null
    };


    var checkLogin = function () {
        sessionObject.isLogged = null;

        sessionObject.currentUser.$get(function (me) {
            sessionObject.isLogged = (!angular.isUndefined(me.id) && me.id > 0);

            if (!sessionObject.isLogged) {
                sessionObject.currentUser = new Account();
            }
            $rootScope.$broadcast('AuthChange');
        }, function () {
			console.log('aa');
            //sessionObject.currentUser = new Account();
            sessionObject.isLogged = false;

            $rootScope.$broadcast('AuthChange');
        });
    };

    checkLogin();

    $rootScope.$on('Auth', checkLogin);

    this.isLogged = function () {
        return sessionObject.isLogged;
    };
    this.getSession = function () {
        return sessionObject;
    };

    this.currentUser = function () {
        return sessionObject.currentUser;
    };

    this.signout = function () {
        Account.signout(function () {
            $rootScope.$broadcast('Auth');
            $rootScope.$broadcast('Signout');
        });
    };
});