app
        .factory('UrlsCache', function ($cacheFactory) {
            return $cacheFactory('urlsCache');
        })
        .factory('Urls', function ($http, UrlsCache) {
            var bases = {'Site': {}};
            var result = {};

            $http.get('/rest/url.json?app=Site', {}, {cache: UrlsCache}).success(function (data) {
                bases['Site'] = data;
                angular.extend(result, {
                    translate: function (data) {
                        var errors = false;
                        if (angular.isUndefined(data.controller) || (typeof data.controller) !== 'string') {
                            console.error('Nie podano wymaganego parametru "controller" lub parametr nie jest stringiem', data);
                            errors = true;
                        } else if (data.controller.indexOf('::') === -1) {
                            console.error('Parametr "controller" musi mieć format <controller>::<action>', data.controller);
                            errors = true;
                        }
                        if (angular.isUndefined(data.app)) {
                            console.error('Nie podano wymaganego parametru "app"', data);
                            errors = true;
                        } else if (angular.isUndefined(bases[data.app])) {
                            console.error('Nieznana aplikacja', data.app);
                        }

                        if (errors) {
                            return;
                        }
                        var urls = bases[data.app].urls;
                        var base = bases[data.app].base;
                        if (!urls || angular.isUndefined(urls[data.controller])) {
                            console.error('Nieznany kontroler dla aplikacji "' + data.app + '"', data.controller, urls, bases);
                            return;
                        }
                        var args = {
                            names: [],
                            values: {},
                            compare: ''
                        };
                        for (var i in data) {
                            if (i === 'app' || i === 'controller') {
                                continue;
                            }
                            args.names.push(i);
                            args.values[i] = data[i];
                        }
                        args.names.sort();
                        args.compare = angular.toJson(args.names);

                        var extractParams = /\(\?P?<[a-z0-9_]+>[^\(\)]+\)/gi;
                        var urlPaterns = urls[data.controller].links;
                        var l = urlPaterns.length;
                        for (var i = 0; i < l; i++) {
                            var pattern = urlPaterns[i];
                            var translations = {};
                            var params = pattern.match(extractParams);
                            var ll = params ? params.length : 0;
                            var names = [];
                            for (var j = 0; j < ll; j++) {
                                var tmp = params[j].match(/<[a-z0-9_]+>/);
                                if (tmp.length === 1) {
                                    var name = tmp[0].replace(/<([a-z0-9_]+)>/, '$1');
                                    names.push(name);
                                    if (!angular.isUndefined(args.values[name])) {
                                        translations[ params[j] ] = args.values[name];
                                    }
                                }
                            }
                            names.sort();

                            if (angular.toJson(names) === args.compare) {
                                var link = pattern;
                                for (var t in translations) {
                                    link = link.replace(t, translations[t]);
                                }
                                link = link.replace(/\\(.?)/g, function (s, n1) {
                                    switch (n1) {
                                        case '\\':
                                            return '\\';
                                        case '0':
                                            return '\u0000';
                                        case '':
                                            return '';
                                        default:
                                            return n1;
                                    }
                                });

                                var template = {
                                    'pattern': pattern,
                                    'params': params,
                                    'names': names,
                                    'translations': translations,
                                    'base': base,
                                    'part': link,
                                    'link': base + link
                                };
                                return template;
                            }

                        }
                        console.error('brak wyników');
                        return '';
                    }
                });
            });

            return result;
        });