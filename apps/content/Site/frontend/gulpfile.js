var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var strip = require('gulp-strip-comments');

var jsFiles = [
    'node_modules/jquery/dist/jquery.js',
    'assets/js/*.js',
    './app/*.js',
    './app/**/*.js'
];

gulp.watch(jsFiles, function (event) {
    if (event.type === 'changed') {
        console.log('przebudowanie skryptów');
        gulp.start('scripts');
    }
});

gulp.task('scripts', function () {
    return gulp.src(jsFiles)
//            .pipe(strip())
            .pipe(concat('scripts.js'))
//            .pipe(uglify())
            .pipe(gulp.dest('./assets'));
});
gulp.task('default', function () {
    // place code for your default task here
});
