<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

if (preg_match('/^(.*)\_x([0-9]{2,4})\.jpg$/', $_SERVER['REQUEST_URI'], $matches)) {
    $save = false;
    $target = $_SERVER['DOCUMENT_ROOT'] . $matches[0];
    $base = $_SERVER['DOCUMENT_ROOT'] . $matches[1];
    $size = (int) $matches[2];

    if (file_exists($base . '.jpg')) {
        $source = $base . '.jpg';
    } elseif (file_exists($base . '.png')) {
        $source = $base . '.jpg';
        $im = new \Imagick($base . '.png');
        $im->setImageBackgroundColor('white');

        $im->setImageFormat('jpg');
        $im->setCompressionQuality(30);
        $im->writeImage($source);
        $im->destroy();
    } else {
        http_response_code(404);
        echo "image source file '{$base}.(png/jpg)' not found!";
        die();
    }

    $im = new \Imagick($source);
    $im->setImageBackgroundColor('white');
    $im->scaleimage($size, 0);
    $im->setImageFormat('jpg');
    $im->setCompressionQuality(30);
    $im->writeImage($target);

    header('Content-Type: image/jpeg');
    echo file_get_contents($target);
    $im->destroy();
} elseif (preg_match('/^(.*)\.jpg$/', $_SERVER['REQUEST_URI'], $matches)) {
    $save = false;
    $base = $_SERVER['DOCUMENT_ROOT'] . $matches[1];

    if (!file_exists($base . '.jpg') && file_exists($base . '.png')) {
        $source = $base . '.jpg';
        $im = new \Imagick($base . '.png');
        $im->setImageBackgroundColor('white');

        $im->setImageFormat('jpg');
        $im->setCompressionQuality(30);
        $im->writeImage($source);

        header('Content-Type: image/jpeg');
        echo file_get_contents($source);
        $im->destroy();
    } else {
        http_response_code(404);
        echo "image source file '{$base}.(png/jpg)' not found!";
        die();
    }
}

http_response_code(404);
echo "hm!";
