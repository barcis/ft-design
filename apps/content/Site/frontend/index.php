<?php

define('ST_FRONTENDDIR', realpath(dirname(__FILE__)));
define('ST_VENDOR', realpath(dirname(__FILE__) . '/../../../../vendor/'));
include_once ST_VENDOR . '/autoload.php';

ob_start("fatal_error_handler");
\Engine5\Core\Engine::initialize();

ini_set('session.cookie_lifetime', 8 * 60 * 60);
ini_set('session.gc_maxlifetime', 8 * 60 * 60);
ini_set("session.gc_probability", 1);
ini_set("session.gc_divisor", 100);

$entrypoint = (\strpos(filter_input(INPUT_SERVER, 'REDIRECT_URL'), '/rest/') === 0) ? \Engine5\Core\Engine\Entrypoint::REST : \Engine5\Core\Engine\Entrypoint::WWW;

$e5 = \Engine5\Core\Engine
        ::getInstance($entrypoint);

$AppName = \Engine5\Core\Engine::getCurrentAppName();
$AppConfig = \Engine5\Core\Engine::getConfig()->configs[$AppName];

if ($AppConfig['database']['type'] === 'yaml') {
    $dbcf = ST_CONFIGDIR . '/' . $AppConfig['database']['file'];
    $dbConfigs = \Engine5\Tools\Yaml::parseFile($dbcf);
    $dbDefault = $dbConfigs['default'];
    $dbConfig = $dbConfigs['databases'][$dbDefault];

    \SORM\Sorm::setDefaultConnection(new \SORM\Config($dbConfig));
}

$e5->Run();

ob_end_flush();

