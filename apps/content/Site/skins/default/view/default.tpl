<!DOCTYPE html>

<html lang="pl" data-ng-app="FTDesignApp">
    <head>
        <title>FT Design - Start</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/assets/css/style.min.css" type="text/css" />
        <link href="/assets/css/zoom.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/microblog.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/node_modules/noty/lib/noty.css" rel="stylesheet" type="text/css"/>
        <script src="/node_modules/angular/angular.min.js" type="text/javascript"></script>
        <script src="/node_modules/angular-resource/angular-resource.min.js" type="text/javascript"></script>
        <script src="/node_modules/noty/lib/noty.min.js" type="text/javascript"></script>
        <script src="/node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/js/jquery.accordion.min.js" type="text/javascript"></script>
        <link href="/assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <script src="/assets/js/jquery.fancybox.js" type="text/javascript"></script>
        [{resources type='app'}]
    </head>

    <body>
        <nav class="container-fluid">
            <ul>
                <li><a href="strona-glowna.html">Start</a></li>
                <li><a href="news.html">Aktualności</a></li>
                <li><a href="gallery.html">Galeria</a></li>
                <li><a href="contact.html">Kontakt</a></li>
            </ul>
        </nav>
        <div class="container">
            <section class="margin-top60">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="logo-triangle">
                            <img class="img-responsive" src="assets/img/logo-triangle.png" alt=""/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-xs-offset-3 col-md-12 col-md-offset-0">
                        <div class="logo-sign">
                            <img class="img-responsive" src="assets/img/logo-sign.png" alt=""/>
                        </div>
                    </div>
                </div>
            </section>
            <div class="facebook" >
                <a href="https://www.facebook.com/FTevents/" target="_blank" rel="nofollow" ><i class="icon-facebook-official"></i></a>
            </div>
        </div>
        [{placeholder name="default"}]
        <div class="container-fluid padding-0">
            <footer>
                <div class="footer-logo" >
                    <p>
                        ©  by <span class="ft-design"><i>FT Design</i></span>
                    </p>
                </div>
                <a href="http://www.batusystems.pl" target="_blank" >Powered by Engine5</a>
            </footer>
        </div>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <script src="/assets/js/scripts.min.js" type="text/javascript"></script>
        [{*        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAkjOu_ZQnW3mih6bJO_eArOqz_tHWyYI&callback=initMap" async defer></script>*}]


    </body>
</html>
