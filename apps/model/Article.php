<?php

namespace NewBatu\Model;

class Article extends Base\Article {

    use \Engine5\Helper\Unique;

    public function publicationDate() {
        return strtotime($this->publication_date);
    }

    public function dateWithMounthName() {
        $englishMonths = array(
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        );
        $polishMonths = array(
            'Stycznia',
            'Lutego',
            'Marca',
            'Kwietnia',
            'Maja',
            'Czerwca',
            'Lipca',
            'Sierpnia',
            'Września',
            'Października',
            'Listopada',
            'Grudnia'
        );
        $articleDate = date_format(date_create($this->publication_date), 'd F Y');
        return str_replace($englishMonths, $polishMonths, $articleDate);
    }

    public function save($withForeign = false) {
        $this->rewrite = $this->createUnique($this->title . '-' . $this->social_id);
        parent::save($withForeign);
    }

}
