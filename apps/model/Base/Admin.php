<?php

namespace NewBatu\Model\Base;

use \SORM\Type\Varchar;
use \SORM\Type\Json;

/**
 * @property-read string $username
 * @property-read string $password
 * @property-read string $firstname
 * @property-read string $lastname
 */
class Admin extends \SORM\Model {

    public static function __definition() {
        parent::__definition();
        self::addColumn(
                (new Varchar('username'))
                        ->setNotNull()
                        ->isUnique(true)
                        ->setSize(128)
        );
        self::addColumn(
                (new Varchar('password'))
                        ->setNotNull()
                        ->setSize(32)
        );
        self::addColumn(
                (new Varchar('firstname'))
                        ->setNotNull()
                        ->setSize(128)
        );
        self::addColumn(
                (new Varchar('lastname'))
                        ->setNotNull()
                        ->setSize(256)
        );
        self::addColumn(
                (new Varchar('email'))
                        ->setNotNull()
                        ->setSize(256)
        );
        self::addColumn(
                (new Json('group'))
        );
    }

}
