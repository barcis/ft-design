<?php

namespace NewBatu\Model\Base;

use \SORM\Type\Varchar,
    \SORM\Type\Text,
    \SORM\Type\Timestamp,
    \SORM\Type\Integer,
    \SORM\Type\Boolean,
    \SORM\Type\Json;

/**
 * @property-read string $title
 * @property-read string $content
 */
class Article extends \SORM\Model {

    public static function __definition() {
        parent::__definition();
        self::addColumn(
                (new Varchar('title'))
                        ->setNotNull()
                        ->isUnique(true)
                        ->setSize(256)
        );

        self::addColumn(
                (new Varchar('rewrite'))
                        ->setNotNull()
                        ->isUnique(true)
                        ->setSize(256)
        );
        self::addColumn(
                (new Text('content'))
                        ->setNotNull()
        );
        self::addColumn(
                (new Json('images'))
        );
        self::addColumn(
                (new Timestamp('publication_date'))
                        ->setNotNull()
                        ->setDefaultValue("'now'::timestamp")
        );
        self::addColumn(
                (new Integer('important'))
                        ->setDefaultValue(0)
        );
        self::addColumn(
                (new Varchar('brief'))
                        ->setSize(240)
        );
        self::addColumn(
                (new Boolean('active'))
                        ->setDefaultValue(true)
        );
        self::addColumn(
                (new Boolean('img_transfer'))
        );
        self::addColumn(
                (new Integer('shows_count'))
                        ->setDefaultValue(0)
        );
        self::addColumn(
                (new Text('source'))
        );
        self::addColumn(
                (new Text('author'))
        );
        self::addColumn(
                (new Json('hashtag'))
        );
        self::addColumn(
                (new Text('social_id'))
        );
        self::addColumn(
                (new Json('params'))
        );
        self::addColumn(
                (new Integer('social_type_id'))
                        ->setNotNull()
                        ->setDefaultValue(1)
                        ->addForeignKeyOneToMany('social_type', 'SocialType', 'id')
        );
    }

}
