<?php

namespace NewBatu\Model\Base;

use SORM\Type\Varchar;
use SORM\Type\Boolean;

/**
 * Description of Tag
 *
 * @property string $name
 * @property string $unique
 * @property int $orderby
 * @author barcis
 */
class Hashtag extends \SORM\Model {

    use \Engine5\Helper\Unique;

    public static function __definition() {
        parent::__definition();
        self::addColumn(
                (new Varchar('name'))
                        ->setNotNull()
                        ->isUnique(true)
        );
        self::addColumn(
                (new Varchar('unique'))
                        ->setNotNull()
                        ->isUnique(true)
        );
        self::addColumn(
                (new Boolean('is_category'))
                        ->setNotNull()
                        ->setDefaultValue(false)
        );
    }

}
