<?php

namespace NewBatu\Model\Base;

use SORM\Type\Timestamp;
use SORM\Type\Integer;

/**
 * Description of Tag
 *
 * @property string $name
 * @property string $unique
 * @property int $orderby
 * @author barcis
 */
class Import extends \SORM\Model {

    use \Engine5\Helper\Unique;

    public static function __definition() {
        parent::__definition();
        self::addColumn(
                (new Integer('social_type_id'))
                        ->setNotNull()
                        ->addForeignKeyOneToMany('type', 'SocialType', 'id')
        );
        self::addColumn(
                (new Timestamp('import_date'))
                        ->setNotNull()
                        ->setDefaultValue("'now'::timestamp")
        );
    }

}
