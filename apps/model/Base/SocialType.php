<?php

namespace NewBatu\Model\Base;

use SORM\Type\Varchar;

/**
 * Description of Tag
 *
 * @property string $name
 * @property string $unique
 * @property int $orderby
 * @author barcis
 */
class SocialType extends \SORM\Model {

    public static function __definition() {
        parent::__definition();
        self::addColumn(
                (new Varchar('social_name'))
                        ->setNotNull()
        );
    }

}
