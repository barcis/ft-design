<?php

namespace NewBatu\Model;

/**
 * Opis dla klasy Tag
 */
class Hashtag extends Base\Hashtag {

    use \Engine5\Helper\Unique;

    public function save($withForeign = false) {
        $this->unique = $this->createUnique($this->name);
        parent::save($withForeign);
    }

}
