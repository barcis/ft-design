#!/usr/bin/php
<?php
define('ST_VENDOR', realpath(__DIR__ . '/../../vendor/'));
define('ST_IMPORT', __DIR__ . '/../../var/import');
define('APPSDIR', realpath(__DIR__ . '/../'));
include_once ST_VENDOR . '/autoload.php';
\Engine5\Core\Engine::initialize();
\Engine5\Core\Engine::getInstance(Engine5\Core\Engine\Entrypoint::SCRIPT);

$dbConfigs = \Engine5\Tools\Yaml::parseFile(APPSDIR . '/content/Site/config/database.config.yml');
$dbDefault = $dbConfigs['default'];
$dbConfig = $dbConfigs['databases'][$dbDefault];

\SORM\Sorm::setDefaultConnection(new \SORM\Config($dbConfig));

set_time_limit(0);
ini_set('memory_limit', '-1');

echo "\n";

if (!file_exists(ST_IMPORT)) {
    mkdir(ST_IMPORT);
    echo "Utworzyłem brakujący katalog " . ST_IMPORT . "\n";
}

$params = [];

if (!empty($argv) && is_array($argv) && count($argv) > 0) {
    foreach ($argv as $arg) {
        $e = explode("=", $arg);

        if (count($e) === 2) {
            $params[strtolower($e[0])] = strtolower($e[1]);
        }
    }
}

$Companies = \NewBatu\Model\Company::q()
        ->whereIsTrue('Company.has_shop')
        ->select(['Company.id', 'Company.name', 'Company.shops']);

echo "Wybrałem {$Companies->count()} firm ze sklepami\n";


foreach ($Companies as $Company /* @var $Company NewBatu\Model\Company  */) {
    echo "Przetwarzam firmę {$Company->name}\n";
    $change = false;
    $shops = $Company->shops;
    foreach ($shops as $index => &$shop) {

        echo " * Przetwarzam sklep {$shop->name}: ";
        if (!isset($shop->xml_url) || empty($shop->xml_url)) {
            echo "pominięto\n";
            continue;
        }

        $change = true;
        $rewrite = \Engine5\Tools\Rewrite::create($shop->name);

        if (!isset($shop->xml_last_download) || (time() - strtotime($shop->xml_last_download)) > 60 * 60 * 24) {
            $xmlcode = file_get_contents($shop->xml_url);
            $shop->xml_filename = ST_IMPORT . '/' . $Company->id . '_' . $rewrite . '.xml';

            file_put_contents($shop->xml_filename, $xmlcode);
            if (!isset($shop->xml_first_download)) {
                $shop->xml_first_download = date("Y-m-d H:i:s");
            }
            $shop->xml_last_download = date("Y-m-d H:i:s");
            echo "POBRANO\n";
        } else {
            echo "nie trzeba pobierać\n";
            $xmlcode = file_get_contents($shop->xml_filename);
        }
        $xml = simplexml_load_string($xmlcode, null, LIBXML_NOCDATA);

        if (isset($xml->group) && isset($xml->group->o)) {
            foreach ($xml->group->o as $product) {

                $productItem = $product->attributes();

                $Product = \NewBatu\Model\Product::q()
                        ->where('affiliate_id', $productItem->id)
                        ->one([
                    'name',
                    'category_id',
                    'description',
                    'price',
                    'value',
                    'url',
                    'affiliate_id',
                    'product_type_id'
                ]);

                if (!$Product) {


                    $photos = [];

                    try {
                        $foto = (string) $product->imgs->main->attributes()->url;
                        $type = pathinfo($foto, PATHINFO_EXTENSION);
                        $photo = file_get_contents($foto);

                        $photos[0] = [
                            'data' => 'data:image/' . $type . ';base64,' . base64_encode($photo)
                        ];
                    } catch (\Exception $ex) {

                    }

                    echo "Dodaje produkt " . $product->name;

                    $Product = \NewBatu\Model\Product::create([
                                'rewrite' => \Engine5\Tools\Rewrite::create($product->name),
                                'program_name' => $Company->name . ' ' . $shop->name,
                                'category_id' => 100,
                                'product_type_id' => 3,
                                'name' => $product->name,
                                'description' => $product->desc,
                                'price' => (float) $productItem->price,
                                'value' => (float) $productItem->price,
                                'affiliate_id' => $productItem->id,
                                'url' => $productItem->url,
                                'photos' => $photos
                    ]);
                } else {
                    echo "Aktualizauję produkt " . $productItem->name;
                    if (!$Product->rewrite) {
                        $Product->rewrite = \Engine5\Tools\Rewrite::create($productItem->name);
                    }
                    $Product->name = $product->name;
                    $Product->category_id = 100;
                    $Product->description = $product->desc;
                    $Product->price = (float) $productItem->price;
                    $Product->value = (float) $productItem->price;
                    $Product->url = $productItem->url;
//                $Product->photos = $photos;
                }

//            if (('Stereofoniczne słuchawki do Xbox One, edycja: Siły Zbrojne' == $productItem->name) || ('Kinect dla konsoli Xbox One' == $productItem->name) || ('Tuner telewizji cyfrowej do konsoli Xbox One' == $productItem->name)
//            ) {
//            echo $Product->getSql();
//            die();
//                continue;
//            }

                $Product->save();
                echo " - done ({$Product->id}) \n";
            }
        }
    }

    $Company->shops = $shops;

    if ($change) {
        $Company->save();
    }
}
