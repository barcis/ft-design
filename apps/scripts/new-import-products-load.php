#!/usr/bin/php
<?php
define('ST_VENDOR', realpath(__DIR__ . '/../../vendor/'));
define('ST_VAR', realpath(__DIR__ . '/../../var/'));
define('ST_APPSDIR', realpath(__DIR__ . '/../'));
define('ST_APPDIR', realpath(ST_APPSDIR . '/content/Site/'));
define('ST_CONFIGDIR', realpath(ST_APPDIR . '/config'));
include_once ST_VENDOR . '/autoload.php';
\Engine5\Core\Engine::initialize();
\Engine5\Core\Engine::getInstance(Engine5\Core\Engine\Entrypoint::SCRIPT);


$dbConfigs = \Engine5\Tools\Yaml::parseFile(ST_CONFIGDIR . '/database.config.yml');
$dbDefault = $dbConfigs['default'];
$dbConfig = $dbConfigs['databases'][$dbDefault];


\SORM\Sorm::setDefaultConnection(new \SORM\Config($dbConfig));

set_time_limit(0);
ini_set('memory_limit', '-1');

$params = [];

if (!empty($argv) && is_array($argv) && count($argv) > 0) {
    foreach ($argv as $arg) {
        $e = explode("=", $arg);

        if (count($e) === 2) {
            $params[strtolower($e[0])] = strtolower($e[1]);
        } elseif (count($e) === 1 && preg_match('/^\-\-/', $e[0])) {
            $params[strtolower(ltrim($e[0], '-'))] = true;
        }
    }
}

$scriptname = basename(__FILE__, '.php');
$fp = fopen('/tmp/' . $scriptname . '.lock', 'w+');
if (!flock($fp, LOCK_EX | LOCK_NB)) {
    echo "\n\nZPN\n\n";
    exit;
}

$program_id = null;
$force = false;

if (isset($params['program_id']) && (int) $params['program_id'] > 0) {
    $program_id = (int) $params['program_id'];
}
if (isset($params['force'])) {
    $force = true;
}

if (!isset($params['source']) || $params['source'] === 'zanox') {
    (new \NewBatu\Admin\Lib\Import\Zanox())->loadAllProducts($program_id, $force);
}
if (!isset($params['source']) || $params['source'] === 'squad') {
    (new \NewBatu\Admin\Lib\Import\NewBatu())->loadAllProducts($program_id, $force);
}
//
//if (!isset($params['source']) || $params['source'] === 'webe') {
//    (new \NewBatu\Admin\Lib\Import\Webe())->loadAllProducts();
//}
//
//if (!isset($params['source']) || $params['source'] === 'sales') {
//    (new \NewBatu\Admin\Lib\Import\SalesMedia())->loadAllProducts();
//}
//if (!isset($params['source']) || $params['source'] === 'tradedoubler') {
//    (new \NewBatu\Admin\Lib\Import\TradeDoubler())->loadAllProducts();
//}
fclose($fp);
