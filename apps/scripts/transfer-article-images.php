#!/usr/bin/php
<?php
define('ST_VENDOR', realpath(__DIR__ . '/../../vendor/'));
define('ST_VAR', realpath(__DIR__ . '/../../var/'));
define('ST_APPSDIR', realpath(__DIR__ . '/../'));
define('ST_APPDIR', realpath(ST_APPSDIR . '/content/Site/'));
define('ST_FRONTENDDIR', realpath(ST_APPSDIR . '/content/Site/frontend/'));
define('ST_CONFIGDIR', realpath(ST_APPDIR . '/config'));
include_once ST_VENDOR . '/autoload.php';
\Engine5\Core\Engine::initialize();
\Engine5\Core\Engine::getInstance(Engine5\Core\Engine\Entrypoint::SCRIPT);

$dbConfigs = \Engine5\Tools\Yaml::parseFile(ST_CONFIGDIR . '/database.config.yml');
$dbDefault = $dbConfigs['default'];
$dbConfig = $dbConfigs['databases'][$dbDefault];

\SORM\Sorm::setDefaultConnection(new \SORM\Config($dbConfig));

set_time_limit(0);
ini_set('memory_limit', '-1');

$params = [];

if (!empty($argv) && is_array($argv) && count($argv) > 0) {
   foreach ($argv as $arg) {
      $e = explode("=", $arg);

      if (count($e) === 2) {
         $params[strtolower($e[0])] = strtolower($e[1]);
      }
   }
}
if (!isset($params['source']) || $params['source'] === 'zanox') {
   (new \NewBatu\Admin\Lib\Images())->transferAritclesImages();
}