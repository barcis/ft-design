<?php

require dirname(__DIR__) . '/../vendor/autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use NewBatu\Site\WebSocket\Main;

$server = IoServer::factory(
                new HttpServer(
                new WsServer(
                new Main()
                )
                )
                , 8080
);
$server->run();
