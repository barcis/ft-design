#!/bin/bash
DIR=`pwd`
touch $DIR/updating.lock

git clone https://barcis@bitbucket.org/barcis/squad.git ./tmp && mv tmp/* ./ && mv tmp/.git* ./
rm -rf tmp/
mkdir var && chmod a+rwx var/

./composer.phar install

rm -f $DIR/updating.lock