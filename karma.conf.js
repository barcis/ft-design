module.exports = function(config) {
    config.set({
        basePath: './',
        files: [
            'apps/content/site/frontend/plugins/bower/jquery/dist/jquery.js',
            'apps/content/site/frontend/plugins/bower/fancybox/source/jquery.fancybox.js',
            'apps/content/site/frontend/plugins/bower/angular/angular.js',
            'apps/content/site/frontend/plugins/bower/angular-resource/angular-resource.js',
            'apps/content/site/frontend/plugins/bower/angular-mocks/angular-mocks.js',
            'apps/content/site/frontend/app/**/*.js',
            'tests/apps/content/site/frontend/unit/**/*.test.js'
        ],
        autoWatch: true,
        frameworks: ['jasmine'],
        browsers: ['Chrome'],
        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine'
        ],
        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        }

    });
};