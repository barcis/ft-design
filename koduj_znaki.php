#!/usr/bin/php
<?php
unset($argv[0]);

$slowo = trim(implode(' ', $argv));

if (strlen($slowo) === 0) {
    die("\nPodaj kurde jakiś słowo!!!\n\n");
}

$result = '';

foreach (str_split($slowo) as $znak) {

    if (preg_match('/[A-Z]/', $znak)) {
        $znak = "Shift+" . strtolower($znak);
    } elseif (' ' === $znak) {
        $znak = "Spacja";
    } elseif (':' === $znak) {
        $znak = "Shift+;";
    }

    $result .= $znak . ":";
}

$result = trim($result, ":");


echo "wynik to: $result\n";

