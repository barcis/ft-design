#!/bin/bash
DIR=`pwd`
touch $DIR/updating.lock

echo "Updating Project" && git pull
echo "Updating Batusystems/SORM" && cd $DIR/vendor/batusystems/sorm/ && git pull
echo "Updating Batusystems/Engine5" && cd $DIR/vendor/batusystems/engine5/ && git pull
echo "Updating Batusystems/E5CMS" && cd $DIR/vendor/batusystems/e5cms/ && git pull
echo "Updating Batusystems/E5CRM" && cd $DIR/vendor/batusystems/e5crm/ && git pull
echo "Updating Batusystems/Config" && cd $DIR/apps/content/Config/ && git pull

rm -f $DIR/updating.lock
