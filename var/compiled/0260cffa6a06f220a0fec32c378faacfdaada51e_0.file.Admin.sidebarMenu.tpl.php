<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:55:48
  from "/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.sidebarMenu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba234eaf549_82793374',
  'file_dependency' => 
  array (
    '0260cffa6a06f220a0fec32c378faacfdaada51e' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.sidebarMenu.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba234eaf549_82793374 ($_smarty_tpl) {
?>
<ul class="sidebar-menu">
    <li class="header">
        Nawigacja
    </li>
    <?php
$_from = $_smarty_tpl->tpl_vars['this']->value->sidebarMenu;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
        <li class="treeview" ui-sref-active="active">
            <a  href data-ui-sref="<?php echo $_smarty_tpl->tpl_vars['item']->value->getState();?>
">
                <i class="fa <?php echo $_smarty_tpl->tpl_vars['item']->value->getIcon();?>
"></i>
                <span><?php echo $_smarty_tpl->tpl_vars['item']->value->getText();?>
</span>
                <?php if ($_smarty_tpl->tpl_vars['item']->value->hasLabel()) {?>
                    <span class="label pull-right <?php echo $_smarty_tpl->tpl_vars['item']->value->getLabel()->getType();?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->getLabel()->getText();?>
</span>
                <?php } else { ?>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                <?php }?>
            </a>
            <?php if ($_smarty_tpl->tpl_vars['item']->value->hasSubItems()) {?>
                <ul class="treeview-menu">
                    <?php
$_from = $_smarty_tpl->tpl_vars['item']->value->getSubItems();
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_subitem_1_saved_item = isset($_smarty_tpl->tpl_vars['subitem']) ? $_smarty_tpl->tpl_vars['subitem'] : false;
$_smarty_tpl->tpl_vars['subitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['subitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['subitem']->value) {
$_smarty_tpl->tpl_vars['subitem']->_loop = true;
$__foreach_subitem_1_saved_local_item = $_smarty_tpl->tpl_vars['subitem'];
?>
                        <li data-ui-sref-active="active">
                            <a href data-ui-sref="<?php echo $_smarty_tpl->tpl_vars['subitem']->value->getState();?>
">
                                <i class="fa <?php echo $_smarty_tpl->tpl_vars['subitem']->value->getIcon();?>
"></i>
                                <span><?php echo $_smarty_tpl->tpl_vars['subitem']->value->getText();?>
</span>
                                <?php if ($_smarty_tpl->tpl_vars['subitem']->value->hasLabel()) {?>
                                    <span class="label pull-right <?php echo $_smarty_tpl->tpl_vars['subitem']->value->getLabel()->getType();?>
"><?php echo $_smarty_tpl->tpl_vars['subitem']->value->getLabel()->getText();?>
</span>
                                <?php }?>
                            </a>
                        </li>
                    <?php
$_smarty_tpl->tpl_vars['subitem'] = $__foreach_subitem_1_saved_local_item;
}
if ($__foreach_subitem_1_saved_item) {
$_smarty_tpl->tpl_vars['subitem'] = $__foreach_subitem_1_saved_item;
}
?>
                </ul>
            <?php }?>
        </li>
    <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
    <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Plugin::plugin(array('hook'=>"admin-sidebar-menu"),$_smarty_tpl);?>

    
</ul><?php }
}
