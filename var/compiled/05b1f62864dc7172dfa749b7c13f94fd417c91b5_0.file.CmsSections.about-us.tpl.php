<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:24:22
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.about-us.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac2e6f1d461_57022307',
  'file_dependency' => 
  array (
    '05b1f62864dc7172dfa749b7c13f94fd417c91b5' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.about-us.tpl',
      1 => 1501748456,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac2e6f1d461_57022307 ($_smarty_tpl) {
?>
<div class="container">
    <section class="text">
        <article>
            <h1>
                <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"title"),$_smarty_tpl);?>

            </h1>
            <div>
                <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"content"),$_smarty_tpl);?>

            </div>
        </article>
    </section>
</div><?php }
}
