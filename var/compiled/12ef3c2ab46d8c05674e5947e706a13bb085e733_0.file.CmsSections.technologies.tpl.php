<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:57:14
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.technologies.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba28a61e367_52062704',
  'file_dependency' => 
  array (
    '12ef3c2ab46d8c05674e5947e706a13bb085e733' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.technologies.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba28a61e367_52062704 ($_smarty_tpl) {
?>
<section class="section">
    <a class="anchor" id="technologies"></a>
    <div class="technologies logo-dotts">
        <h1><?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"title"),$_smarty_tpl);?>
</h1>
        <h3><?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"subtitle"),$_smarty_tpl);?>
</h3>
        <div class="container-fluid">
            <div class="row">
                <?php
$_from = $_smarty_tpl->tpl_vars['this']->value->sections;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cmsSection_0_saved_item = isset($_smarty_tpl->tpl_vars['cmsSection']) ? $_smarty_tpl->tpl_vars['cmsSection'] : false;
$_smarty_tpl->tpl_vars['cmsSection'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cmsSection']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cmsSection']->value) {
$_smarty_tpl->tpl_vars['cmsSection']->_loop = true;
$__foreach_cmsSection_0_saved_local_item = $_smarty_tpl->tpl_vars['cmsSection'];
?>
                    <div class="col-md-4">
                        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'NewBatu\Site\Region\CmsSections','view'=>$_smarty_tpl->tpl_vars['cmsSection']->value->type->name,'section'=>$_smarty_tpl->tpl_vars['cmsSection']->value),$_smarty_tpl);?>

                    </div>
                <?php
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_local_item;
}
if ($__foreach_cmsSection_0_saved_item) {
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_item;
}
?>
            </div>
        </div>
    </div>
    <a href="#contact" class="scrolldown"></a>
</section><?php }
}
