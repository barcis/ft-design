<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:55:45
  from "/home/yuliia/Work/ft-design/apps/content/Admin/skins/default/view/nologged.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba2310173b4_53417663',
  'file_dependency' => 
  array (
    '1f497580d7ba7bd1e37dca30f095deef0df59f1f' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Admin/skins/default/view/nologged.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba2310173b4_53417663 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <base href="/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>AdminLTE 2 | Log in</title>
        <link href="/assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/jvectormap/jquery-jvectormap.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/iCheck/skins/square/blue.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="/">
                    <img src="http://squad.com.pl/assets/img/logo.png" alt="squad"/>
                </a>
            </div>
            <div class="login-box-body">
                <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'\NewBatu\Admin\Region\Admin','view'=>'login'),$_smarty_tpl);?>

            </div>
        </div>

        <?php echo '<script'; ?>
 src="/assets/plugins/jquery/dist/jquery.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap/dist/js/bootstrap.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/iCheck/icheck.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });
            });
        <?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
