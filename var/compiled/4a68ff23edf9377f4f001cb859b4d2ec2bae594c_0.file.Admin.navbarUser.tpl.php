<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:55:48
  from "/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.navbarUser.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba234e8dca2_28062757',
  'file_dependency' => 
  array (
    '4a68ff23edf9377f4f001cb859b4d2ec2bae594c' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.navbarUser.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba234e8dca2_28062757 ($_smarty_tpl) {
?>
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="/assets/img/user2-160x160.jpg" class="user-image" alt="User Image">
        <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['this']->value->user->firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['this']->value->user->lastname;?>
</span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <img src="/assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">

            <p>
                <?php echo $_smarty_tpl->tpl_vars['this']->value->user->firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['this']->value->user->lastname;?>

                <small>Member since Nov. 2012</small>
            </p>
        </li>
        <!-- Menu Body -->
        
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="/admins/edit-<?php echo $_smarty_tpl->tpl_vars['this']->value->user->id;?>
" class="btn btn-default btn-flat">Profil</a>
            </div>
            <div class="pull-right">
                <a target="_self" href="/logowanie/logout.html" class="btn btn-default btn-flat">Wyloguj</a>
            </div>
        </li>
    </ul>
</li><?php }
}
