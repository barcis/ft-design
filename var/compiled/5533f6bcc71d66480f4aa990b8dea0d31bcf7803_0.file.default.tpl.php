<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:48:44
  from "/home/yuliia/Work/ft-design/apps/content/Site/skins/default/view/default.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac89c56c617_52572611',
  'file_dependency' => 
  array (
    '5533f6bcc71d66480f4aa990b8dea0d31bcf7803' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/skins/default/view/default.tpl',
      1 => 1503303027,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac89c56c617_52572611 ($_smarty_tpl) {
?>
<!DOCTYPE html>

<html lang="pl" data-ng-app="FTDesignApp">
    <head>
        <title>FT Design - Start</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/assets/css/style.min.css" type="text/css" />
        <link href="/assets/css/zoom.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/microblog.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/node_modules/noty/lib/noty.css" rel="stylesheet" type="text/css"/>
        <?php echo '<script'; ?>
 src="/node_modules/angular/angular.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/node_modules/angular-resource/angular-resource.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/node_modules/noty/lib/noty.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/node_modules/jquery/dist/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/js/jquery.accordion.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <link href="/assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <?php echo '<script'; ?>
 src="/assets/js/jquery.fancybox.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Resources::render(array('type'=>'app'),$_smarty_tpl);?>

    </head>

    <body>
        <nav class="container-fluid">
            <ul>
                <li><a href="strona-glowna.html">Start</a></li>
                <li><a href="news.html">Aktualności</a></li>
                <li><a href="gallery.html">Galeria</a></li>
                <li><a href="contact.html">Kontakt</a></li>
            </ul>
        </nav>
        <div class="container">
            <section class="margin-top60">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="logo-triangle">
                            <img class="img-responsive" src="assets/img/logo-triangle.png" alt=""/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-xs-offset-3 col-md-12 col-md-offset-0">
                        <div class="logo-sign">
                            <img class="img-responsive" src="assets/img/logo-sign.png" alt=""/>
                        </div>
                    </div>
                </div>
            </section>
            <div class="facebook" >
                <a href="https://www.facebook.com/FTevents/" target="_blank" rel="nofollow" ><i class="icon-facebook-official"></i></a>
            </div>
        </div>
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Placeholder::render(array('name'=>"default"),$_smarty_tpl);?>

        <div class="container-fluid padding-0">
            <footer>
                <div class="footer-logo" >
                    <p>
                        ©  by <span class="ft-design"><i>FT Design</i></span>
                    </p>
                </div>
                <a href="http://www.batusystems.pl" target="_blank" >Powered by Engine5</a>
            </footer>
        </div>
        <div id="fb-root"></div>
        <?php echo '<script'; ?>
>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/js/scripts.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        


    </body>
</html>
<?php }
}
