<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:57:14
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.portfolio-item.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba28a6179c0_45143421',
  'file_dependency' => 
  array (
    '6398397616cca39741795c43a615e71aa3447c35' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.portfolio-item.tpl',
      1 => 1496407433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba28a6179c0_45143421 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/home/yuliia/Work/ft-design/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('ifcontent', array('name'=>"name")); $_block_repeat=true; echo Engine5\Core\Templater\Smarty\Plugins\Blocks\IfContent::render(array('name'=>"name"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<div class="item" data-name="<?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"name"),$_smarty_tpl);?>
">
    <figure>
        <picture>
            <source srcset="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['this']->value->content->logo->data,'.png','_x800.jpg');?>
" media="(min-width: 992px)">
            <source srcset="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['this']->value->content->logo->data,'.png','_x550.jpg');?>
" media="(min-width: 768px) and (max-width: 991px)">
            <source srcset="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['this']->value->content->logo->data,'.png','_x700.jpg');?>
" media="(min-width: 641px) and (max-width: 767px)">
            <source srcset="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['this']->value->content->logo->data,'.png','_x650.jpg');?>
" media="(min-width: 465px) and (max-width: 640px)">
            <source srcset="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['this']->value->content->logo->data,'.png','_x400.jpg');?>
" media="(min-width: 249px) and (max-width: 464px)">
            <img src="<?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"logo"),$_smarty_tpl);?>
" alt="<?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"name"),$_smarty_tpl);?>
"/>
        </picture>
    </figure>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Engine5\Core\Templater\Smarty\Plugins\Blocks\IfContent::render(array('name'=>"name"), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
