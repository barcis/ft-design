<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:48:44
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.gallery.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac89c58e985_29116243',
  'file_dependency' => 
  array (
    '66064a200b335e25e3f5a4e280f1fce923d76efa' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.gallery.tpl',
      1 => 1503313784,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac89c58e985_29116243 ($_smarty_tpl) {
?>
<section class="container-fluid">
    <div class="row">
        <div class="gallery">
            <?php
$_from = $_smarty_tpl->tpl_vars['this']->value->sections;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cmsSection_0_saved_item = isset($_smarty_tpl->tpl_vars['cmsSection']) ? $_smarty_tpl->tpl_vars['cmsSection'] : false;
$_smarty_tpl->tpl_vars['cmsSection'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cmsSection']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cmsSection']->value) {
$_smarty_tpl->tpl_vars['cmsSection']->_loop = true;
$__foreach_cmsSection_0_saved_local_item = $_smarty_tpl->tpl_vars['cmsSection'];
?>
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3" >
                    <div class="item" data-name="photo">
                        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'NewBatu\Site\Region\CmsSections','view'=>$_smarty_tpl->tpl_vars['cmsSection']->value->type->name,'section'=>$_smarty_tpl->tpl_vars['cmsSection']->value),$_smarty_tpl);?>

                    </div>
                </div>
            <?php
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_local_item;
}
if ($__foreach_cmsSection_0_saved_item) {
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_item;
}
?>

        </div>
    </div>
</section><?php }
}
