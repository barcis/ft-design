<?php
/* Smarty version 3.1.29, created on 2017-08-21 09:57:03
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.contact.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599a924f463ad8_96691714',
  'file_dependency' => 
  array (
    '962c042ec53889ef3b1e084c7b40dc50b987b899' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.contact.tpl',
      1 => 1502187181,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599a924f463ad8_96691714 ($_smarty_tpl) {
?>
<section class="container-fluid padding-0">

    <div id="map">
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" title="Google Maps" aria-label="Google Maps" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$PAGES_CONTAINER.$centeredContent.$inlineContent.$SITE_PAGES.$cfvg_DESKTOP.$inlineContent.$comp-j1ot5gw3.$mapContainer.0" src="http://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1235.281125492315!2d19.50109012317801!3d51.76238401017762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471bcb4dd82d1999%3A0xb1352c2a0e9b2740!2zUGnFgnN1ZHNraWVnbyAxMzUsIMWBw7Nkxbo!5e0!3m2!1spl!2spl!4v1501758261625"></iframe>
    </div>

</section>

<div class="container">
    <section ng-controller="Controllers.Contact.SendContact">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6">

                <div class="col-xs-12 col-sm-6 col-md-6">
                    <span class="name-header"> Mateusz Kamiński </span>
                    <span class="name-position">General Director</span>

                    Mobile: +48 668 912 766, <br>
                    Mail: m.kaminski@ft-design.eu
                </div>
                <div class="col-xs-12 col-sm-6  col-md-6 mar-top30">
                    <span class="name-header"> Wanda Jankowska</span>
                    <span class="name-position">General Manager</span>

                    Mobile: +48 790 731 998,<br>
                    Mail: w.jankowska@ft-design.eu
                </div>

                <!--                        <div class="col-md-12">
                                            <a href="https://www.facebook.com/FTevents/" target="_blank" rel="nofollow" class="facebook-contact">
                                                <i class="icon-facebook-official"></i>

                                            </a>
                                        </div>-->

                <div class="col-md-12 ">
                    

                    <span class="name-form">Formularz kontaktowy <span class="ft-design"><i>FT Design</i></span></span>

                    <form method="post" novalidate>
                        <input type="text" class="form-control"  placeholder="Imię" name="sender" data-ng-model="ask.sender" required>
                        <input type="email" class="form-control"  placeholder="Email" name="senderEmail" data-ng-model="ask.senderEmail" required>
                        <input type="text" class="form-control"  placeholder="Firma" name="company" data-ng-model="ask.company">
                        <input type="phone" class="form-control"  placeholder="Telefon" name="phone" data-ng-model="ask.phone">
                        <input type="text" class="form-control"  placeholder="Temat" name="topic" data-ng-model="ask.topic">
                        <textarea class="form-control big"  placeholder="Wiadomość" name="message" data-ng-model="ask.message" required></textarea>
                        <button  type="submit" data-ng-click="submit()"  for="submit">
                            Wyślij
                        </button>
                    </form>
                </div>

            </div>

            <div class=" col-md-6 triangle-background">

            </div>

        </div>
    </section>
</div><?php }
}
