<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:47:13
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/Menu/Menu.default.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba031275185_90568948',
  'file_dependency' => 
  array (
    'a2b958947a424e8df1791dfd2e92347deef27e0e' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/Menu/Menu.default.tpl',
      1 => 1496327629,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba031275185_90568948 ($_smarty_tpl) {
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed  page-scroll" data-toggle="collapse" data-target="#hamburger" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="logo" href="/">
                <img src="/assets/img/Logo-main.png" alt="Logo BATUSystems"/>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="hamburger">
            <div class=" nav navbar-nav navbar-right">
                <ul>
                    <li><a class="page-scroll" href="#home">Główna</a></li>
                    <li><a class="page-scroll" href="#about-us">O firmie</a></li>
                    <li><a class="page-scroll" href="#offer">Oferta</a></li>
                    <li><a class="page-scroll" href="#portfolio">Realizacje</a></li>
                    <li><a class="page-scroll" href="#technologies">Technologie</a></li>
                    <li><a class="page-scroll" href="#contact">Kontakt</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav><?php }
}
