<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:55:45
  from "/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba231022031_56295648',
  'file_dependency' => 
  array (
    'abf5d00c55fbdeb2c50a7bb1d571075e454ffe21' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Admin/Region/views/Admin/Admin.login.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba231022031_56295648 ($_smarty_tpl) {
?>
<p class="login-box-msg">Zaloguj się aby rozpocząć</p>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>'login-form')); $_block_repeat=true; echo Engine5\Core\Templater\Smarty\Plugins\Blocks\Form::render(array('name'=>'login-form'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<div class="form-group has-feedback">
    <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Forms::render(array('name'=>'username','class'=>'form-control','placeholder'=>'Email','autofocus'=>'autofocus'),$_smarty_tpl);?>

    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Forms::render(array('name'=>'password','class'=>"form-control",'placeholder'=>"Password"),$_smarty_tpl);?>

    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
    <div class="col-xs-8">
        <div class="checkbox icheck">
            <label>
                <input type="checkbox"> Zapamiętaj mnie
            </label>
        </div>
    </div>
    <div class="col-xs-4">
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Forms::render(array('name'=>'doLogin','class'=>"btn btn-primary btn-block btn-flat"),$_smarty_tpl);?>

    </div>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Engine5\Core\Templater\Smarty\Plugins\Blocks\Form::render(array('name'=>'login-form'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<a href="#">Zapomniałem hasło</a><br><?php }
}
