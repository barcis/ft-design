<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:55:48
  from "/home/yuliia/Work/ft-design/apps/content/Admin/skins/default/view/default.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba234e79a84_16582356',
  'file_dependency' => 
  array (
    'de1a3a6378b04b8fea04c7f4921806282fb893e4' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Admin/skins/default/view/default.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba234e79a84_16582356 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="pl" data-ng-app="NewBatuAdminApp">
    <head>
        <meta charset="UTF-8">
        <base href="/">
        <title>BATUadmin | Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="/assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/components-font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/jvectormap/jquery-jvectormap.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/angular-chart.js/dist/angular-chart.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/PACE/pace.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/ui-select/dist/select.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/nestable.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/batubox/batubox.css" rel="stylesheet" type="text/css" />

        <link href="/assets/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <![endif]-->
        <?php echo '<script'; ?>
 src="//cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="//cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
>
            webshims.setOptions('waitReady', false);
            webshims.polyfill('forms forms-ext');
        <?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/jquery/dist/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/noty/js/noty/packaged/jquery.noty.packaged.js" type="text/javascript"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 src="/assets/plugins/angular/angular.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js" type="text/javascript" ><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap3-wysihtml5-bower/dist/templates.js" type="text/javascript" ><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap3-wysihtml5-bower/dist/locales/bootstrap-wysihtml5.en-US.js" type="text/javascript" ><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap3-wysihtml5-bower/dist/locales/bootstrap-wysihtml5.pl-PL.js" type="text/javascript" ><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-permission/dist/angular-permission.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-permission/dist/angular-permission-ui.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/moment/moment.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-moment/angular-moment.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <!-- [app] -->
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Resources::render(array('type'=>'app'),$_smarty_tpl);?>

        <!-- [/app] -->
        <!-- [apps] -->
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Resources::render(array('type'=>'apps'),$_smarty_tpl);?>

        <!-- [/apps] -->

        <!-- [js] -->
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Resources::render(array('type'=>'js'),$_smarty_tpl);?>

        <!-- [/js] -->
        <!-- [css] -->
        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Resources::render(array('type'=>'css'),$_smarty_tpl);?>

        <!-- [/css] -->

    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="/" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>S</b>quad</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>SQUAD</b>admin</span>
                </a>

                <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'\NewBatu\Admin\Region\Admin','view'=>'navbar'),$_smarty_tpl);?>


            </header>
            <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'\NewBatu\Admin\Region\Admin','view'=>'sidebar'),$_smarty_tpl);?>

            <div class="content-wrapper">
                <section class="content-header" data-ui-view="headerView"></section>
                <section class="content" data-ui-view="mainView"></section>
            </div>

            <footer class="main-footer">
                <img class="product" src="/assets/img/batusystems.png" alt="batusystems"/>
                <b>Wersja:</b> 1.0.0
                <div class="pull-right hidden-xs">
                    <strong>Copyright &copy; 2010-2017.</strong> Wszelkie prawa zastrzeżone.
                    <ul class="logotypes">
                        <li>
                            <a href="http://batusystems.pl" target="_blank" rel="nofollow">
                                <img class="batusystems" src="/assets/img/batusystems.png" alt="batusystems"/>
                            </a>
                        </li>
                        <li>
                            <a href="http://engine5.pl" target="_blank" rel="nofollow">
                                <img class="engine5" src="/assets/img/engine5.png" alt="engine5"/>
                            </a>
                        </li>
                        <li>
                            <a href="http://cms.engine5.pl" target="_blank" rel="nofollow">
                                <img class="cms" src="/assets/img/cms.png" alt="cms"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li>
                        <a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-wrench"></i></a>
                    </li>
                    <li>
                        <a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a>
                    </li>
                    <li>
                        <a href="#control-sidebar-stats-tab" data-toggle="tab"><i class="fa fa-wrench"></i></a>
                    </li>
                    <li>
                        <a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->


                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <?php echo '<script'; ?>
 src="/assets/plugins/lodash/dist/lodash.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-resource/angular-resource.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/ui-router-extras/release/ct-ui-router-extras.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-noty/dist/angular-noty.dist.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/ng-contextmenu/dist/ng-contextmenu.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootbox/bootbox.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/batubox/batubox.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/ngBootbox/dist/ngBootbox.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/iCheck/icheck.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/Chart.js/Chart.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/fastclick/lib/fastclick.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/jquery.slimscroll/jquery.slimscroll.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-chart.js/dist/angular-chart.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/jquery-sparkline/dist/jquery.sparkline.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/jvectormap/jquery.jvectormap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/jvectormap/tests/assets/jquery-jvectormap-world-mill-en.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/PACE/pace.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/nestable/jquery.nestable.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/cryptico/cryptico.js"type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/danialfarid-angular-file-upload/dist/angular-file-upload.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/danialfarid-angular-file-upload/dist/angular-file-upload-html5-shim.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/fileinput/fileinput.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/ui-select/dist/select.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/select2/select2.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/select2/select2_locale_pl.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-ui-select2/src/select2.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-sanitize/angular-sanitize.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/ngstorage/ngStorage.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-batubox/angular-batubox.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/assets/plugins/angular-google-maps/dist/angular-google-maps.js" type="text/javascript"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 src="/assets/js/app.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAiyymIme1P5BSWNvcvtLhSuajnZxQ--ys"
        type="text/javascript"><?php echo '</script'; ?>
>
        
    </body>
</html>
<?php }
}
