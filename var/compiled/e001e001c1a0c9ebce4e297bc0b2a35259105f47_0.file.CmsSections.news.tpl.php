<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:40:01
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.news.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac691f00d62_96741628',
  'file_dependency' => 
  array (
    'e001e001c1a0c9ebce4e297bc0b2a35259105f47' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.news.tpl',
      1 => 1503314984,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac691f00d62_96741628 ($_smarty_tpl) {
?>

<div class="parallax photo-news"></div>
<div class="container" data-ng-controller="Controllers.News.List">


    <div class=" microblog">
        <div ng-repeat="item in Model" class="microblog-item" ng-switch="item.type" >
            <div ng-switch-when="photo">
                <div class="picture">
                    <img class="fb-img" alt="{{item.name}}" src="{{item.full_picture}}">
                </div>
                <div class="item-title">
                    <div class="panelTitle fitText" style="font-size: 14px; margin-bottom: 5px; padding-left: 10px; margin-top: 5px">{{item.name}}</div>
                </div>
                <div class="desc">
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.description}} </div>
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.message}} </div>
                </div>
            </div>
            <div ng-switch-when="video">
                <div class="picture">
                    <iframe width="286" height="160" frameborder="0" allowfullscreen autoplay="0" data-ng-src="{{item.trustedSource}}"></iframe>
                </div>
                <div class="item-title">
                    <div class="panelTitle fitText" style="font-size: 14px; margin-bottom: 5px; padding-left: 10px; margin-top: 5px">{{item.name}}</div>
                </div>
                <div class="desc">
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.description}} </div>
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.message}} </div>
                </div>
            </div>
            <div ng-switch-when="ststus">
                <div class="item-title">
                    <div class="panelTitle fitText" style="font-size: 14px; margin-bottom: 5px; padding-left: 10px; margin-top: 5px">{{item.name}}</div>
                </div>
                <div class="desc">
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.description}} </div>
                    <div class="panelText fitText" style="font-size: 12px; margin-top: 5px"> {{item.message}} </div>
                </div>
            </div>
            <div class="social">
                <span>Udostępnij</span>
                <a class="shareLink google" target="_blank" href="https://plus.google.com/share?url={{item.link}};mode=page"><i class="fa fa-google-plus fa-lg" style="color:#CB2028"></i></a>
                <a class="shareLink twitter" target="_blank" href="http://twitter.com/share?text=&amp;url={{item.link}};mode=page"><i class="fa fa-twitter fa-lg" style="color:#00aced"></i></a>
                <a class="shareLink fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{item.link}};mode=page"><i class="fa fa-facebook fa-lg" style="color:#3b5998"></i></a>
            </div>
        </div>
    </div>
</div>
<?php }
}
