<?php
/* Smarty version 3.1.29, created on 2017-06-22 12:57:14
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.portfolio.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_594ba28a57b525_16882302',
  'file_dependency' => 
  array (
    'e89cece906ce9e3d281402a7e56263f91a602eb8' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.portfolio.tpl',
      1 => 1496054736,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594ba28a57b525_16882302 ($_smarty_tpl) {
?>
<section class="section">
    <a class="anchor" id="portfolio"></a>
    <div class="container-fluid">
        <div class="portfolio logo-dotts">
            <div class="row">
                <h1 class="col-xs-12"><?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Content::render(array('name'=>"title"),$_smarty_tpl);?>
</h1>
            </div>
            <div class="row">
                <?php
$_from = $_smarty_tpl->tpl_vars['this']->value->sections;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cmsSection_0_saved_item = isset($_smarty_tpl->tpl_vars['cmsSection']) ? $_smarty_tpl->tpl_vars['cmsSection'] : false;
$_smarty_tpl->tpl_vars['cmsSection'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cmsSection']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cmsSection']->value) {
$_smarty_tpl->tpl_vars['cmsSection']->_loop = true;
$__foreach_cmsSection_0_saved_local_item = $_smarty_tpl->tpl_vars['cmsSection'];
?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'NewBatu\Site\Region\CmsSections','view'=>$_smarty_tpl->tpl_vars['cmsSection']->value->type->name,'section'=>$_smarty_tpl->tpl_vars['cmsSection']->value),$_smarty_tpl);?>

                    </div>
                <?php
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_local_item;
}
if ($__foreach_cmsSection_0_saved_item) {
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_item;
}
?>
            </div>
        </div>
    </div>
    <a href="#technologies" class="scrolldown"></a>
</section><?php }
}
