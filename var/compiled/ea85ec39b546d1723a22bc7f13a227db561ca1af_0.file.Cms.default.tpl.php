<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:48:44
  from "/home/yuliia/Work/ft-design/vendor/batusystems/e5cms/Region/views/Cms/Cms.default.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac89c588fe2_56181997',
  'file_dependency' => 
  array (
    'ea85ec39b546d1723a22bc7f13a227db561ca1af' => 
    array (
      0 => '/home/yuliia/Work/ft-design/vendor/batusystems/e5cms/Region/views/Cms/Cms.default.tpl',
      1 => 1503309518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac89c588fe2_56181997 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['this']->value->cmsPage && ($_smarty_tpl->tpl_vars['this']->value->cmsPage->type === 'cms' || $_smarty_tpl->tpl_vars['this']->value->cmsPage->type === 'template' || $_smarty_tpl->tpl_vars['this']->value->cmsPage->type === 'master') && $_smarty_tpl->tpl_vars['this']->value->cmsPage->sections && count($_smarty_tpl->tpl_vars['this']->value->cmsPage->sections) > 0) {?>
    <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Subsections::render(array(),$_smarty_tpl);?>

<?php } elseif ($_smarty_tpl->tpl_vars['this']->value->cmsPage && $_smarty_tpl->tpl_vars['this']->value->cmsPage->type === 'region') {?>
    <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>$_smarty_tpl->tpl_vars['this']->value->cmsPage->region,'view'=>$_smarty_tpl->tpl_vars['this']->value->cmsPage->view),$_smarty_tpl);?>

<?php }
}
}
