<?php
/* Smarty version 3.1.29, created on 2017-08-21 13:24:22
  from "/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.slider.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_599ac2e6f225f1_27784765',
  'file_dependency' => 
  array (
    'fd0ef8377d21af30b6cfa3f0b48f75453fd6a49b' => 
    array (
      0 => '/home/yuliia/Work/ft-design/apps/content/Site/Region/views/CmsSections/CmsSections.slider.tpl',
      1 => 1501751100,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599ac2e6f225f1_27784765 ($_smarty_tpl) {
?>
<section class="batu-acordeon hidden-md hidden-sm hidden-xs">
    <div class="mySlider">
        <ul>
            <?php
$_from = $_smarty_tpl->tpl_vars['this']->value->sections;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cmsSection_0_saved_item = isset($_smarty_tpl->tpl_vars['cmsSection']) ? $_smarty_tpl->tpl_vars['cmsSection'] : false;
$_smarty_tpl->tpl_vars['cmsSection'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cmsSection']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cmsSection']->value) {
$_smarty_tpl->tpl_vars['cmsSection']->_loop = true;
$__foreach_cmsSection_0_saved_local_item = $_smarty_tpl->tpl_vars['cmsSection'];
?>
                <?php echo Engine5\Core\Templater\Smarty\Plugins\Functions\Region::render(array('name'=>'NewBatu\Site\Region\CmsSections','view'=>$_smarty_tpl->tpl_vars['cmsSection']->value->type->name,'section'=>$_smarty_tpl->tpl_vars['cmsSection']->value),$_smarty_tpl);?>

            <?php
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_local_item;
}
if ($__foreach_cmsSection_0_saved_item) {
$_smarty_tpl->tpl_vars['cmsSection'] = $__foreach_cmsSection_0_saved_item;
}
?>
        </ul>
    </div>
</section>
<?php }
}
